import React, { Component } from "react";
import { Text, View, Modal, Image, TouchableOpacity, Alert } from "react-native";
import { connect } from 'react-redux';

import Table from "../models/Table";
import Order from "../scenes/order";
import { colors } from "../styles/colors";
import { fonts } from "../styles/app";
import Api from "../services/api";
import Comanda from "../models/Comanda";
import LargeModal from "./large_modal";

class OrdersBar extends Component {
    constructor(props) {
        super(props);

        this.state = {
            table: null,
            modalVisible: false,
            comandas: this.props.comandas,
        };

        this.closeModal = this.closeModal.bind(this);
    }

    componentDidMount() {
        Table.getTable().then(table => {
            this.setState({ table });
        });
    }

    hasOrders() {
        var empty = true;

        for (var c of this.state.comandas) {
            if (c.products && c.products.length !== 0) {
                empty = false;
            }
        }

        return !empty;
    }

    ordersCount() {
        var count = 0;

        console.log(this.props.comandas);

        for (var c of this.props.comandas) {
            if (c.products && c.products.length !== 0) {
                for (var p of c.products) {
                    count += p.quantity;
                }
            }
        }

        return count;
    }

    showOrders() {
        this.setState({ modalVisible: true });
    }

    closeModal() {
        this.setState({ modalVisible: false });
    }

    callWaiter() {
        Alert.alert(
            "CONFIRMAR",
            "Tem certeza que deseja chamar o Garçom?",
            [
                { text: "Não" },
                {
                    text: "Sim",
                    onPress: () => {
                        Alert.alert(
                            "AGUARDE",
                            "O garçom foi chamado e virá até a sua mesa logo.",
                            [{ text: "OK" }],
                            { cancelable: false }
                        );

                        Table.getTable().then(table => {
                            Api.get("call-waiter/" + table);
                        });
                    }
                }
            ],
            { cancelable: false }
        );
    }

    render() {
        if (this.props.mini) {
            return (
                <View style={styles.containerMini}>
                    <TouchableOpacity onPress={() => this.callWaiter()}>
                        <View style={styles.buttonMini}>
                            <View style={[styles.iconContainerMini, { backgroundColor: colors.tertiary }]}>
                                <Image
                                    source={require("../img/waiter_bell.png")}
                                    style={[styles.iconMini, { tintColor: colors.onTertiary }]}
                                />
                            </View>

                            <Text style={styles.textMini}>CHAMAR O GARÇOM</Text>
                        </View>
                    </TouchableOpacity>

                    {this.ordersCount() !== 0 && (
                        <TouchableOpacity onPress={() => this.showOrders()}>
                            <View style={styles.buttonMini}>
                                <View
                                    style={[styles.iconContainerMini, { backgroundColor: colors.secondary }]}
                                >
                                    <Image
                                        source={require("../img/tray.png")}
                                        style={[styles.iconMini, { tintColor: "#fff" }]}
                                    />

                                    <View style={styles.badgeMini}>
                                        <Text style={styles.badgeTextMini}>{this.ordersCount()}</Text>
                                    </View>
                                </View>

                                <Text style={styles.textMini}>MEU PEDIDO</Text>
                            </View>
                        </TouchableOpacity>
                    )}

                    <LargeModal visible={this.state.modalVisible} closeModal={this.closeModal}>
                        <Order closeModal={this.closeModal} />
                    </LargeModal>
                </View>
            );
        } else {
            return (
                <View style={styles.container}>
                    {this.props.comandas && this.props.comandas.products && this.props.comandas.products.length !== 0 && (
                        <TouchableOpacity onPress={() => this.showOrders()}>
                            <View style={styles.button}>
                                <View style={[styles.iconContainer, { backgroundColor: colors.secondary }]}>
                                    <Image
                                        source={require("../img/tray.png")}
                                        style={[styles.icon, { tintColor: "#fff" }]}
                                    />
                                </View>

                                <Text style={styles.text}>MEU PEDIDO</Text>

                                <View style={styles.badge}>
                                    <Text style={styles.badgeText}>{this.ordersCount()}</Text>
                                </View>
                            </View>
                        </TouchableOpacity>
                    )}

                    <TouchableOpacity onPress={() => this.callWaiter()}>
                        <View style={styles.button}>
                            <View style={[styles.iconContainer, { backgroundColor: colors.tertiary }]}>
                                <Image
                                    source={require("../img/waiter_bell.png")}
                                    style={[styles.icon, { tintColor: colors.onTertiary }]}
                                />
                            </View>

                            <Text style={styles.text}>CHAMAR O GARÇOM</Text>
                        </View>
                    </TouchableOpacity>

                    <LargeModal visible={this.state.modalVisible} closeModal={this.closeModal}>
                        <Order closeModal={this.closeModal} />
                    </LargeModal>
                </View>
            );
        }
    }
}

export default connect(
    state => ({ comandas: state.comandasReducer.comandas }),
    {}
)(OrdersBar);

const styles = {
    container: {
        position: "absolute",
        top: 24,
        right: 16,
        zIndex: 999
    },
    containerMini: {
        flexDirection: "row"
    },
    button: {
        width: 80,
        alignItems: "center",
        marginBottom: 32,
        position: "relative"
    },
    buttonMini: {
        flexDirection: "row",
        alignItems: "center",
        marginLeft: 32
    },
    iconContainer: {
        height: 64,
        width: 64,
        borderRadius: 64,
        justifyContent: "center",
        alignItems: "center",
        marginBottom: 8,
        elevation: 2
    },
    iconContainerMini: {
        height: 32,
        width: 32,
        borderRadius: 32,
        justifyContent: "center",
        alignItems: "center",
        marginRight: 8,
        position: "relative"
    },
    icon: {
        height: 32,
        resizeMode: "contain"
    },
    iconMini: {
        height: 16,
        resizeMode: "contain"
    },
    textShadow: {
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 20
        },
        shadowOpacity: 0.4,
        elevation: 2
    },
    text: {
        fontFamily: fonts.secondary,
        color: "#fff",
        fontSize: 16,
        textAlign: "center"
    },
    textMini: {
        fontFamily: fonts.secondary,
        color: colors.defaultText,
        fontSize: 16
    },
    badge: {
        position: "absolute",
        top: 0,
        right: 0,
        width: 24,
        height: 24,
        borderRadius: 24,
        backgroundColor: colors.primary,
        justifyContent: "center",
        alignItems: "center"
    },
    badgeMini: {
        position: "absolute",
        top: -4,
        right: -4,
        width: 16,
        height: 16,
        borderRadius: 16,
        backgroundColor: colors.primary,
        justifyContent: "center",
        alignItems: "center"
    },
    badgeText: {
        color: colors.onPrimary,
        textAlign: "center",
        fontFamily: fonts.secondary,
        fontSize: 14
    },
    badgeTextMini: {
        color: colors.onPrimary,
        textAlign: "center",
        fontFamily: fonts.secondary,
        fontSize: 8
    }
};
