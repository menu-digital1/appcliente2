import React, { Component } from 'react'
import { Alert, TouchableOpacity, Text, View } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

import Table from '../models/Table';
import Api from '../services/api';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';

export default class WaiterCall extends Component {

    callWaiter() {
        Alert.alert('AGUARDE', 'O garçom foi chamado e virá até a sua mesa logo.', [
            { text: 'OK' }
        ], { cancelable: false });

        Table.getTable().then(table => {
            Api.get('call-waiter/' + table);
        });
    }

    render() {
        return (
            <TouchableOpacity style={styles.container} onPress={() => this.callWaiter()}>
                <View style={styles.iconContainer}>
                    <Icon name="bell-o" style={styles.icon} />
                </View>

                <Text style={styles.text}>CHAMAR O GARÇOM</Text>
            </TouchableOpacity>
        )
    }
}

const styles = {
    iconContainer: {
        width: 42,
        height: 42,
        borderRadius: 42,
        backgroundColor: colors.primaryColor,
        justifyContent: 'center',
        alignItems: 'center',
        textAlign: 'center',
        marginBottom: 5,
        alignSelf: 'center',
    },
    icon: {
        color: colors.textInPrimary,
        fontSize: 28,
    },
    text: {
        color: colors.primaryColor,
        fontSize: 11,
        fontFamily: fonts.secondary,
        textAlign: 'center',
    }
}