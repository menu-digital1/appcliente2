import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import { colors } from '../styles/colors';
import { fonts } from '../styles/app';

export default class NumberSpinner extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value: props.value,
			step: props.step ? props.step : 1,
			minReached: false,
			maxReached: false
		};
	}

	componentDidMount() {
		var value = this.props.value;
		var maxReached = false;
		var minReached = false;

		if (this.props.max !== null && value >= this.props.max) {
			value = this.props.max;
			maxReached = true;
		}

		if (this.props.min !== null && value <= this.props.min) {
			value = this.props.min;
			minReached = true;
		}

		this.setState({
			value,
			maxReached,
			minReached
		});
	}

	dec() {
		if (!this.state.minReached) {
			this.setState(
				{
					minReached: false,
					maxReached: false,
					value: this.state.value - this.state.step
				},
				() => {
					if (this.props.min !== null) {
						if (this.state.value < this.props.min) {
							this.setState({
								minReached: true,
								value: this.props.min
							});

							return false;
						} else if (this.state.value == this.props.min) {
							this.setState({
								minReached: true,
								value: this.props.min
							});
						}
					}

					this.props.onChange(this.state.value);
				}
			);
		}
	}

	inc() {
		if (!this.state.maxReached) {
			this.setState(
				{
					minReached: false,
					maxReached: false,
					value: this.state.value + this.state.step
				},
				() => {
					if (this.props.max !== null) {
						if (this.state.value > this.props.max) {
							this.setState({
								maxReached: true,
								value: this.props.max
							});

							return false;
						} else if (this.state.value == this.props.max) {
							this.setState({
								maxReached: true,
								value: this.props.max
							});
						}
					}

					this.props.onChange(this.state.value);
				}
			);
		}
	}

	render() {
		return (
			<View style={styles.container}>
				<TouchableOpacity onPress={() => this.dec()}>
					<View
						style={[
							styles.decContainer,
							this.state.minReached ? styles.disabled : {}
						]}
					>
						<Icon name='minus' style={styles.decIcon} />
					</View>
				</TouchableOpacity>

				<View style={styles.valueContainer}>
					<Text style={styles.value}>{this.state.value}</Text>
				</View>

				<TouchableOpacity onPress={() => this.inc()}>
					<View
						style={[
							styles.incContainer,
							this.state.maxReached ? styles.disabled : {}
						]}
					>
						<Icon name='plus' style={styles.incIcon} />
					</View>
				</TouchableOpacity>
			</View>
		);
	}
}

const styles = {
	container: {
		flexDirection: 'row',
		height: 50
	},
	valueContainer: {
		backgroundColor: '#D7D7D7',
		paddingHorizontal: 40,
		height: '100%',
		justifyContent: 'center',
		alignItems: 'center'
	},
	value: {
		fontFamily: fonts.default,
		fontSize: 18,
		color: colors.defaultText,
		textAlign: 'center'
	},
	decContainer: {
		backgroundColor: '#C9C8C8',
		borderTopLeftRadius: 4,
		borderBottomLeftRadius: 4,
		height: '100%',
		paddingHorizontal: 20,
		justifyContent: 'center'
	},
	decIcon: {
		fontSize: 18,
		color: colors.defaultText
	},
	incContainer: {
		backgroundColor: colors.secondaryColor,
		paddingHorizontal: 20,
		borderTopRightRadius: 4,
		borderBottomRightRadius: 4,
		height: '100%',
		justifyContent: 'center'
	},
	incIcon: {
		fontSize: 18,
		color: colors.textInSecondary
	},
	disabled: {
		opacity: 0.4
	}
};
