import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import numeral from 'numeral';
import Icon from 'react-native-vector-icons/FontAwesome';

import Table from '../models/Table';
import { fonts } from '../styles/app';
import { colors } from '../styles/colors';

export default class TableBar extends Component {
	constructor(props) {
		super(props);

		this.state = {
			table: null
		};
	}

	componentDidMount() {
		Table.getTable().then(table => {
			this.setState({ table });
		});
	}

	render() {
		return (
			<View style={styles.container}>
				{this.props.onBack && (
					<TouchableOpacity
						onPress={this.props.onBack}
						hitSlop={{
							top: 40,
							left: 40,
							bottom: 40,
							right: 40
						}}
					>
						<Icon name='arrow-left' style={styles.backIcon} />
					</TouchableOpacity>
				)}

				<Text style={styles.tableNumber}>MESA {numeral(this.state.table).format('00')}</Text>
			</View>
		);
	}
}

const styles = {
	container: {
		flexDirection: 'row',
		paddingVertical: 24,
		alignItems: 'center'
	},
	tableNumber: {
		flex: 1,
		textAlign: 'center',
		fontFamily: fonts.secondary,
		fontSize: 26,
		color: colors.onPrimary
	},
	backIcon: {
		fontSize: 24,
		color: colors.onPrimary,
		marginLeft: 16
	}
};
