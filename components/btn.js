import React, { Component } from 'react';
import { View, Text, TouchableOpacity, ActivityIndicator } from 'react-native';

import { colors } from '../styles/colors';
import { fonts } from '../styles/app';

export default class Btn extends Component {
	constructor(props) {
		super(props);

		this.state = {};
	}

	render() {
		buttonColor = null;
		buttonDisabledColor = null;
		textColor = null;

		switch (this.props.type) {
			case 'primary':
				buttonColor = colors.primary;
				buttonDisabledColor = colors.primary;
				textColor = colors.onPrimary;

				break;

			case 'secondary':
				buttonColor = colors.secondaryColor;
				buttonDisabledColor = colors.secondaryLightColor;
				textColor = colors.textInSecondary;

				break;

			default:
				buttonColor = colors.tertiary;
				buttonDisabledColor = colors.onTertiary;
				textColor = colors.onTertiary;
		}

		align = this.props.align ? this.props.align : 'center';

		if (this.props.loading) {
			return <ActivityIndicator size='large' style={{ height: 48 }} color={buttonColor} />;
		} else if (this.props.disabled) {
			return (
				<View
					style={[
						styles.button,
						{
							opacity: 0.4,
							backgroundColor: buttonColor,
							alignSelf: align
						}
					]}
				>
					<Text style={[styles.text, { color: textColor }]}>{this.props.text}</Text>
				</View>
			);
		} else {
			return (
				<TouchableOpacity onPress={this.props.onPress} style={[styles.button, { backgroundColor: buttonColor, alignSelf: align }]}>
					<Text style={[styles.text, { color: textColor }]}>{this.props.text.toUpperCase()}</Text>
				</TouchableOpacity>
			);
		}
	}
}

const styles = {
	button: {
		borderRadius: 2,
		height: 48,
		paddingHorizontal: 40,
		justifyContent: 'center',
		alignItems: 'center',
		minWidth: 64
	},
	text: {
		textAlign: 'center',
		fontFamily: fonts.secondary,
		fontSize: 18
	}
};
