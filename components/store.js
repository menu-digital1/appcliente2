import { createStore, combineReducers } from 'redux';

import comandasReducer from '../reducers/comandas_reducer';

export default createStore(combineReducers({
    comandasReducer,
}));