import React, { Component } from 'react';
import { Modal, View, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

export default class SmallModal extends Component {
    render() {
        return (
            <Modal
                animationType='fade'
                transparent
                onRequestClose={this.props.closeModal}
                onBackButtonPress={this.props.closeModal}
                visible={this.props.visible}
            >
                <TouchableWithoutFeedback onPress={this.props.closeModal}>
                    <View style={styles.modal}>
                        <TouchableWithoutFeedback>
                            <View style={styles.modalContent}>
                                <View style={styles.modalHeader}>
                                    <TouchableOpacity
                                        onPress={this.props.closeModal}
                                        hitSlop={{
                                            top: 40,
                                            left: 40,
                                            bottom: 40,
                                            right: 40
                                        }}
                                    >
                                        <Icon name='times' style={styles.closeIcon} />
                                    </TouchableOpacity>
                                </View>

                                <View style={styles.modalBody} onStartShouldSetResponder={() => true}>
                                    {this.props.children}
                                </View>
                            </View>
                        </TouchableWithoutFeedback>
                    </View>
                </TouchableWithoutFeedback>
            </Modal>
        );
    }
}

const styles = {
    modal: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalContent: {
        backgroundColor: '#fff',
        borderRadius: 5,
        width: 770
    },
    modalHeader: {
        justifyContent: 'flex-end',
        flexDirection: 'row'
    },
    closeIcon: {
        color: '#BABABA',
        fontSize: 26,
        padding: 10
    },
    modalBody: {
        padding: 40,
        paddingTop: 15
    }
};
