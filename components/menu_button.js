import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';

import { colors } from '../styles/colors';
import { fonts } from '../styles/app';

export default class MenuButton extends Component {
	render() {
		return (
			<TouchableOpacity onPress={this.props.onPress}>
				<View style={[styles.container, this.props.active ? styles.activeContainer : {}]}>
					{this.props.icon && (
						<Icon name={this.props.icon} fixedWidth style={[styles.menuIcon, this.props.active ? styles.activeIcon : {}]} />
					)}

					<Text style={[styles.menuText, this.props.active ? styles.activeText : {}]}>{this.props.text.toUpperCase()}</Text>
				</View>
			</TouchableOpacity>
		);
	}
}

const styles = {
	container: {
		backgroundColor: colors.surface,
		marginBottom: 24,
		flexDirection: 'row',
		alignItems: 'center',
		paddingVertical: 16,
		paddingHorizontal: 56,
		borderRadius: 2
	},
	activeContainer: {
		backgroundColor: colors.secondary
	},
	menuIcon: {
		color: colors.onSurface,
		fontSize: 18,
		marginRight: 16
	},
	activeIcon: {
		colors: colors.onSecondary
	},
	menuText: {
		color: colors.onSurface,
		fontSize: 20,
		fontFamily: fonts.secondary
	},
	activeText: {
		color: colors.onSecondary
	}
};
