import React, { Component } from 'react'
import { Text, View, Animated, Easing } from 'react-native'

import { fonts } from '../styles/app';

export default class DonutLoading extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
            
        };

        const spinValue = new Animated.Value(0);

        Animated.loop(Animated.timing(
            spinValue,
            {
                toValue: 1,
                duration: 1000,
                easing: Easing.inOut(Easing.cubic),
                useNativeDriver: true,
            }
        )).start();

        this.spin = spinValue.interpolate({
            inputRange: [0, 1],
            outputRange: ['0deg', '720deg']
        });
    }

    render() {
        return (
            <View style={styles.loadingContainer}>
                <Animated.Image source={require('../img/donut.png')} style={[styles.loadingIcon, { transform: [{ rotate: this.spin }] }]} />

                <Text style={styles.loadingText}>CARREGANDO</Text>
            </View>
        )
    }
}

const styles = {
    loadingContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    loadingIcon: {
        height: 64,
        opacity: 0.2,
        marginBottom: 15,
        resizeMode: 'contain',
    },
    loadingText: {
        textAlign: 'center',
        fontSize: 24,
        fontFamily: fonts.secondary,
        color: '#bababa',
    }
}