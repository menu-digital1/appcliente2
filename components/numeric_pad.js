import React, { Component } from 'react';
import { View, Text, TouchableOpacity, Image, ActivityIndicator } from 'react-native';

import { fonts } from '../styles/app';
import { colors } from '../styles/colors';

export default class NumericPad extends Component {
	constructor(props) {
		super(props);

		this.state = {
			value: props.value
		};
	}

	addValue(value) {
		this.setState({ value: `${this.state.value}${value}` }, () => {
			this.props.onChange(this.state.value);
		});
	}

	backspace() {
		this.setState({ value: this.state.value.slice(0, -1) }, () => {
			this.props.onChange(this.state.value);
		});
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.visor}>
					<Text style={styles.visorText}>{this.state.value}</Text>
				</View>

				<View style={styles.keyboardContainer}>
					<View style={styles.row}>
						<View style={styles.cell}>
							<TouchableOpacity onPress={() => this.addValue('1')}>
								<View style={styles.button}>
									<Text style={styles.buttonText}>1</Text>
								</View>
							</TouchableOpacity>
						</View>

						<View style={styles.cell}>
							<TouchableOpacity onPress={() => this.addValue('2')}>
								<View style={styles.button}>
									<Text style={styles.buttonText}>2</Text>
								</View>
							</TouchableOpacity>
						</View>

						<View style={styles.cell}>
							<TouchableOpacity onPress={() => this.addValue('3')}>
								<View style={styles.button}>
									<Text style={styles.buttonText}>3</Text>
								</View>
							</TouchableOpacity>
						</View>
					</View>

					<View style={styles.row}>
						<View style={styles.cell}>
							<TouchableOpacity onPress={() => this.addValue('41')}>
								<View style={styles.button}>
									<Text style={styles.buttonText}>4</Text>
								</View>
							</TouchableOpacity>
						</View>

						<View style={styles.cell}>
							<TouchableOpacity onPress={() => this.addValue('5')}>
								<View style={styles.button}>
									<Text style={styles.buttonText}>5</Text>
								</View>
							</TouchableOpacity>
						</View>

						<View style={styles.cell}>
							<TouchableOpacity onPress={() => this.addValue('6')}>
								<View style={styles.button}>
									<Text style={styles.buttonText}>6</Text>
								</View>
							</TouchableOpacity>
						</View>
					</View>

					<View style={styles.row}>
						<View style={styles.cell}>
							<TouchableOpacity onPress={() => this.addValue('7')}>
								<View style={styles.button}>
									<Text style={styles.buttonText}>7</Text>
								</View>
							</TouchableOpacity>
						</View>

						<View style={styles.cell}>
							<TouchableOpacity onPress={() => this.addValue('8')}>
								<View style={styles.button}>
									<Text style={styles.buttonText}>8</Text>
								</View>
							</TouchableOpacity>
						</View>

						<View style={styles.cell}>
							<TouchableOpacity onPress={() => this.addValue('9')}>
								<View style={styles.button}>
									<Text style={styles.buttonText}>9</Text>
								</View>
							</TouchableOpacity>
						</View>
					</View>

					<View style={styles.row}>
						<View style={styles.cell}></View>

						<View style={styles.cell}>
							<TouchableOpacity onPress={() => this.addValue('0')}>
								<View style={styles.button}>
									<Text style={styles.buttonText}>0</Text>
								</View>
							</TouchableOpacity>
						</View>

						<View style={styles.cell}>
							<TouchableOpacity onPress={() => this.backspace()}>
								<View style={styles.clearButton}>
									<Image source={require('../img/backspace.png')} style={styles.buttonImg} />
								</View>
							</TouchableOpacity>
						</View>
					</View>

					<View style={styles.row}>
						<View style={styles.cell}></View>

						<View style={styles.cell}>
							{!this.props.loading && (
								<TouchableOpacity onPress={this.props.onSubmit}>
									<View style={styles.okButton}>
										<Text style={styles.okButtonText}>OK</Text>
									</View>
								</TouchableOpacity>
							)}

							{this.props.loading && <ActivityIndicator style={{ height: 56 }} size='large' color={colors.tertiary} />}
						</View>

						<View style={styles.cell}></View>
					</View>
				</View>
			</View>
		);
	}
}

const styles = {
	container: {},
	visor: {
		height: 80,
		justifyContent: 'center',
		alignItems: 'center',
		padding: 16,
		backgroundColor: colors.surface,
		borderWidth: 1,
		borderColor: colors.divider,
		borderRadius: 4,
		marginBottom: 16
	},
	visorText: {
		fontFamily: fonts.default,
		fontSize: 32,
		textAlign: 'center',
		color: colors.onSurface
	},
	keyboardContainer: {},
	row: {
		flexDirection: 'row',
		marginHorizontal: -8
	},
	cell: {
		flex: 1,
		padding: 8
	},
	button: {
		justifyContent: 'center',
		alignItems: 'center',
		padding: 8,
		backgroundColor: colors.surface,
		borderWidth: 1,
		borderColor: colors.divider,
		borderRadius: 4,
		height: 80
	},
	clearButton: {
		justifyContent: 'center',
		alignItems: 'center',
		padding: 8,
		backgroundColor: colors.surface,
		height: 80
	},
	buttonText: {
		fontFamily: fonts.default,
		fontSize: 32,
		color: colors.onSurface,
		textAlign: 'center'
	},
	okButton: {
		height: 56,
		borderRadius: 2,
		backgroundColor: colors.tertiary,
		justifyContent: 'center',
		alignItems: 'center'
	},
	okButtonText: {
		fontFamily: fonts.secondary,
		color: colors.onTertiary,
		fontSize: 20
	},
	buttonImg: {
		height: 48,
		resizeMode: 'contain',
		tintColor: colors.onSurface
	}
};
