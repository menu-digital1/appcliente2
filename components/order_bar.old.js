import React, { Component } from 'react';
import { Text, View, Modal, Image, TouchableOpacity, Alert } from 'react-native';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/FontAwesome';

import Table from '../models/Table';
import Order from '../scenes/order';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';
import Api from '../services/api';
import LargeModal from './large_modal';

class OrdersBar extends Component {
	constructor(props) {
		super(props);

		this.state = {
			table: null,
			modalVisible: false
		};

		this.closeModal = this.closeModal.bind(this);
	}

	componentDidMount() {
		Table.getTable().then(table => {
			this.setState({ table });
		});
	}

	hasOrders() {
		var empty = true;

		for (var c of this.props.comandas) {
			if (c.products && c.products.length !== 0) {
				empty = false;
			}
		}

		return !empty;
	}

	ordersCount() {
		var count = 0;

		for (var c of this.props.comandas) {
			if (c.products && c.products.length !== 0) {
				for (var p of c.products) {
					count += p.quantity;
				}
			}
		}

		return count;
	}

	showOrders() {
		this.setState({ modalVisible: true });
	}

	closeModal() {
		this.setState({ modalVisible: false });
	}

	callWaiter() {
		Alert.alert(
			'CONFIRMAR',
			'Tem certeza que deseja chamar o Garçom?',
			[
				{ text: 'Não' },
				{
					text: 'Sim',
					onPress: () => {
						Alert.alert('AGUARDE', 'O garçom foi chamado e virá até a sua mesa logo.', [{ text: 'OK' }], { cancelable: false });

						Table.getTable().then(table => {
							Api.get('call-waiter/' + table);
						});
					}
				},
			],
			{ cancelable: false }
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.tableContainer}>
					{this.props.onBack && (
						<TouchableOpacity
							style={styles.backButton}
							onPress={this.props.onBack}
							hitSlop={{
								top: 40,
								left: 40,
								bottom: 40,
								right: 40
							}}
						>
							<Icon name='arrow-left' style={styles.backIcon} />
						</TouchableOpacity>
					)}

					<Text style={styles.tableNumber}>MESA {this.state.table}</Text>
				</View>

				<View style={styles.subContainer}>
					<View style={styles.logoContainer}>
						{this.props.showLogo && <Image source={require('../img/logo_limpa.png')} style={styles.logo} />}
					</View>

					<View style={styles.toolbar}>
						<TouchableOpacity onPress={() => this.callWaiter()}>
							<View style={styles.waiterButton}></View>
						</TouchableOpacity>

						{this.hasOrders() && (
							<TouchableOpacity onPress={() => this.showOrders()}>
								<View style={styles.ordersButtonContainer}>
									<View style={styles.ordersButton}></View>

									<View style={styles.ordersBadgeContainer}>
										<Text style={styles.ordersBadge}>{this.ordersCount()}</Text>
									</View>
								</View>
							</TouchableOpacity>
						)}

						{!this.hasOrders() && <View style={styles.noOrdersButton}></View>}
					</View>
				</View>

				<LargeModal visible={this.state.modalVisible} closeModal={this.closeModal}>
					<Order closeModal={this.closeModal} />
				</LargeModal>
			</View>
		);
	}
}

export default connect(
	state => ({
		comandas: state.comandasReducer.comandas
	}),
	{}
)(OrdersBar);

const styles = {
	container: {
		flexDirection: 'row',
		backgroundColor: 'rgba(38, 38, 38, 0.5)',
		alignItems: 'center',
		position: 'absolute',
		top: 0,
		left: 0,
		right: 0,
		zIndex: 9999,
		height: 80
	},
	subContainer: {
		flexDirection: 'row',
		alignItems: 'center',
		justifyContent: 'space-between',
		paddingVertical: 15,
		paddingHorizontal: 40,
		flex: 1
	},
	tableContainer: {
		width: 360,
		paddingVertical: 15,
		paddingHorizontal: 40,
		alignItems: 'center',
		flexDirection: 'row'
	},
	tableNumber: {
		color: '#fff',
		fontSize: 20,
		fontFamily: fonts.secondary
	},
	logoContainer: {
		height: '100%'
	},
	logo: {
		height: '100%',
		resizeMode: 'contain'
	},
	itemsContainer: {
		flex: 1,
		flexDirection: 'row',
		alignItems: 'center'
	},
	itemsText: {
		fontFamily: fonts.secondary,
		fontSize: 14,
		color: colors.defaultText,
		marginHorizontal: 10
	},
	backButton: {
		marginRight: 15
	},
	backIcon: {
		color: '#fff',
		fontSize: 22
	},
	toolbar: {
		flexDirection: 'row'
	},
	waiterButton: {
		backgroundColor: colors.secondaryColor,
		width: 50,
		height: 60,
		borderRadius: 2,
		marginRight: 50
	},
	ordersButtonContainer: {
		position: 'relative'
	},
	ordersButton: {
		backgroundColor: '#fff',
		width: 50,
		height: 60,
		borderRadius: 2
	},
	ordersBadgeContainer: {
		width: 24,
		height: 24,
		borderRadius: 24,
		backgroundColor: colors.primaryColor,
		justifyContent: 'center',
		alignItems: 'center',
		position: 'absolute',
		bottom: 0,
		right: 0
	},
	ordersBadge: {
		color: colors.textInPrimary,
		textAlign: 'center',
		fontFamily: fonts.default,
		fontSize: 13
	},
	noOrdersButton: {
		backgroundColor: 'rgba(255, 255, 255, 0.3)',
		width: 50,
		height: 60,
		borderRadius: 2
	}
};
