import React, { Component } from 'react'
import { TouchableOpacity } from 'react-native'
import Icon from 'react-native-vector-icons/FontAwesome';

export default class Radio extends Component {
    render() {
        return (
            <TouchableOpacity 
                onPress={this.props.onPress} 
                hitSlop={{
                    top: 40,
                    left: 40,
                    bottom: 40,
                    right: 40
                }}
            >
                <Icon 
                    name={this.props.value == true ? 'check-circle' : 'circle-o'}
                    style={[styles.icon, { color: this.props.color }]}
                />
            </TouchableOpacity>
        )
    }
}

const styles = {
    icon: {
        fontSize: 30,
    }
}