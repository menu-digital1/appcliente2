import React, { Component } from 'react'
import { Text, View, Image } from 'react-native'

import { fonts } from '../styles/app';

export default class Bell extends Component {
    render() {
        badgeColor = '#ab1e35';
        badgeText = '#fff';

        switch (this.props.style) {
            case 'secondary':
                badgeColor = '#4e4e4e';
                badgeText = '#fff';

                break;
        }

        return (
            <View style={styles.container}>
                <Image source={require('../img/bell.png')} style={styles.icon} />

                {
                    this.props.number &&
                    <View style={[styles.badge, { backgroundColor: badgeColor }]}>
                        <Text style={[styles.badgeText, { color: badgeText }]}>{this.props.number}</Text>
                    </View>
                }
            </View>
        )
    }
}

const styles = {
    container: {
        position: 'relative',
        width: 40,
        height: 40,
    },
    icon: {
        height: '100%',
        width: '100%',
        resizeMode: 'contain',
    },
    badge: {
        width: 20,
        height: 20,
        borderRadius: 20,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'absolute',
        top: 0,
        right: 0,
    },
    badgeText: {
        textAlign: 'center',
        fontSize: 12,
        fontFamily: fonts.secondary,
    }
}