export const updateComandas = (comandas) => {
    return {
        type: 'update_comandas',
        payload: comandas
    };
};