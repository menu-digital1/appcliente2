const INITIAL_STATE = {
    comandas: [],
};
 
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case 'update_comandas':
            return { ...state, comandas: action.payload };
        default:
            return state;
    }
};
