import React, { Component } from 'react';
import { FlatList, View, ScrollView, Alert, Text, TouchableOpacity } from 'react-native';
import NumericInput from 'react-native-numeric-input';
import Icon from 'react-native-vector-icons/FontAwesome';
import _ from 'lodash';
import { connect } from 'react-redux';
import numeral from 'numeral';
import ImageLoad from 'react-native-image-placeholder';

import { updateComandas } from '../reducers/actions';
import Comanda from '../models/Comanda';
import Btn from '../components/btn';
import WaiterCall from '../components/waiter_call';
import { fonts } from '../styles/app';
import { colors } from '../styles/colors';
import NumberSpinner from '../components/number_spinner';
import Radio from '../components/radio';
import VariantsComandaItem from './variants_comanda_item';
import VariantHeader from './variant_header';

class Variants extends Component {
    constructor(props) {
        super(props);

        this.state = {
            currentComanda: 0,
            comandas: this.props.comandas.slice(),
            productQuantity: 0,
            variantsQuantity: {},
            variantsChoice: {},
            currentProduct: {},
            productValue: 0,
            loading: false,
            total: 0,
            variantsReachedBottom: false
        };

        this.setCurrentComanda = this.setCurrentComanda.bind(this);
        this.setComandas = this.setComandas.bind(this);
    }

    componentWillMount() {
        // var comandas = Comanda.getComandas();

        // this.setState(
        // 	{
        // 		comandas,
        // 		currentComanda: comandas[0]
        // 	},
        // 	() => {
        // 		this.getProductQuantity();
        // 	}
        // );
        console.log(this.props.comandas);
    }

    setCurrentComanda(comanda) {
        this.setState({ currentComanda: comanda });
    }

    setProductQuantity(value) {
        var data = {
            id: this.props.product._id,
            name: this.props.product.nome,
            value: this.props.product.valor_final,
            tipo: this.props.product.tipo,
            quantity: value,
            total: value * this.props.product.valor_final
        };

        var comandas = this.state.comandas;

        if (!comandas[this.state.currentComanda].products) {
            comandas[this.state.currentComanda].products = [];
        }

        var productIndex = _.findIndex(comandas[this.state.currentComanda].products, p => p.id == this.props.product._id);

        if (productIndex == -1) {
            comandas[this.state.currentComanda].products.push(data);
        } else {
            if (value <= 0) {
                comandas[this.state.currentComanda].products.splice(productIndex, 1);
            } else {
                comandas[this.state.currentComanda].products[productIndex] = {
                    id: data.id,
                    name: data.name,
                    value: data.value,
                    tipo: data.tipo,
                    quantity: data.quantity,
                    total: data.quantity * data.value,
                    variants: comandas[this.state.currentComanda].products[productIndex].variants
                };
            }
        }

        this.setComandas(comandas);
    }

    getProductQuantity() {
        var productQuantity = 0;

        if (this.state.comandas[this.state.currentComanda].products) {
            var product = _.find(this.state.comandas[this.state.currentComanda].products, p => p.id == this.props.product._id);

            if (product) {
                productQuantity = product.quantity;
            }
        }

        return productQuantity;
    }

    getProductValue() {
        var productValue = 0;

        if (this.state.comandas[this.state.currentComanda].products) {
            var product = _.find(this.state.comandas[this.state.currentComanda].products, p => p.id == this.props.product._id);

            if (product) {
                productValue = product.value * product.quantity;

                if (product.variants) {
                    for (var v of product.variants) {
                        if (v.quantity) {
                            productValue += v.quantity * v.value * product.quantity;
                        } else {
                            if (v.selected) {
                                productValue += v.value * product.quantity;
                            }
                        }
                    }
                }
            }
        }

        return productValue;
    }

    finalize() {
        this.props.closeModal();
    }

    isEmptyOrder() {
        return !(this.state.currentComanda.products && this.state.currentComanda.products.length !== 0);
    }

    isCloseToBottom = ({ layoutMeasurement, contentOffset, contentSize }) => {
        return layoutMeasurement.height + contentOffset.y >= contentSize.height - 1;
    };

    setComandas(comandas) {
        this.props.updateComandas(comandas);
        this.setState({ comandas });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.menuContainer}>
                    <Text style={styles.h2}>SELECIONE A COMANDA PARA EFETUAR O PEDIDO:</Text>

                    <View style={styles.comandasList}>
                        <FlatList
                            extraData={this.state}
                            data={this.state.comandas}
                            contentContainerStyle={styles.comandasListContainer}
                            keyExtractor={(i, key) => key.toString()}
                            renderItem={({ item, index }) => (
                                <VariantsComandaItem
                                    comanda={item}
                                    comandas={this.state.comandas}
                                    index={index}
                                    currentComanda={this.state.currentComanda}
                                    setCurrentComanda={this.setCurrentComanda}
                                />
                            )}
                        />
                    </View>
                </View>

                <View style={styles.content}>
                    <View style={styles.productContainer}>
                        <ImageLoad
                            source={{
                                uri: this.props.product.full_gif ? this.props.product.full_gif : this.props.product.full_imagem
                            }}
                            isShowActivity={false}
                            style={styles.productImage}
                        />

                        <View style={styles.productInfo}>
                            <Text style={styles.productTitle}>{this.props.product.nome}</Text>

                            <Text style={styles.productDescription}>{this.props.product.descricao}</Text>

                            <Text style={styles.productValue}>
                                Valor unitário:{' '}
                                <Text style={{ fontWeight: 'bold' }}>{numeral(this.props.product.valor_final).format('$ 0,0.00')}</Text>
                            </Text>
                        </View>
                    </View>

                    <Text style={styles.instructions}>
                        SELECIONE A QUANTIDADE PARA A COMANDA {numeral(this.props.comandas[this.state.currentComanda].number).format('00')}:
                    </Text>

                    <View style={styles.productToolbar}>
                        <NumberSpinner onChange={value => this.setProductQuantity(value)} value={this.getProductQuantity()} min={0} />

                        <Text style={styles.productTotalValue}>{`Total: ${numeral(this.getProductValue()).format('$ 0,0.00')}`}</Text>
                    </View>

                    <View style={styles.variantsContainer}>
                        {this.getProductQuantity() == 0 && (
                            <View style={styles.emptyOverlay}>
                                {/* <Text style={styles.emptyOverlayText}>Entre com a quantidade</Text> */}
                            </View>
                        )}

                        <Text style={styles.instructions}>SELECIONE OS ADICIONAIS:</Text>

                        <FlatList
                            extraData={this.state}
                            data={this.props.variants}
                            keyExtractor={(i, key) => key.toString()}
                            renderItem={i => (
                                <VariantHeader
                                    variant={i.item}
                                    currentComanda={this.state.currentComanda}
                                    product={this.props.product}
                                    comandas={this.state.comandas}
                                    setComandas={this.setComandas}
                                    // productQuantity={this.state.productQuantity}
                                />
                            )}
                            style={styles.variantsListContainer}
                            scrollEnabled={this.getProductQuantity() > 0}
                            onScroll={({ nativeEvent }) => {
                                if (this.isCloseToBottom(nativeEvent)) {
                                    this.setState({ variantsReachedBottom: true });
                                } else {
                                    this.setState({ variantsReachedBottom: false });
                                }
                            }}
                        />

                        <Icon
                            name='chevron-down'
                            style={[styles.scrollIndicator, { color: this.state.variantsReachedBottom ? 'transparent' : colors.gray }]}
                        />
                    </View>

                    <View style={styles.actionBar}>
                        <Btn text='FINALIZAR' onPress={() => this.finalize()} />
                    </View>
                </View>
            </View>
        );
    }
}

export default connect(
    state => ({
        comandas: state.comandasReducer.comandas
    }),
    {
        updateComandas
    }
)(Variants);

const styles = {
    container: {
        flex: 1,
        flexDirection: 'row'
    },
    innerContainer: {
        flexDirection: 'row',
        flex: 1
    },
    menuContainer: {
        width: 380,
        backgroundColor: '#F7F2F2',
        paddingTop: 60,
        paddingBottom: 40,
        marginTop: -60,
        marginLeft: -40,
        marginBottom: -40,
        borderTopLeftRadius: 5,
        borderBottomLeftRadius: 5
    },
    content: {
        flex: 1,
        paddingLeft: 40
    },
    variantsListContainer: {
        flex: 1
    },
    header: {},
    comandaButton: {
        paddingVertical: 20,
        paddingHorizontal: 25,
        flexDirection: 'row',
        alignItems: 'center'
    },
    comandaMarker: {
        marginRight: 20
    },
    comandaNumber: {
        fontFamily: fonts.default,
        fontSize: 14,
        fontWeight: 'bold'
    },
    emptyOverlay: {
        position: 'absolute',
        backgroundColor: 'rgba(255, 255, 255, 0.8)',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 99
    },
    emptyOverlayText: {
        color: '#bababa',
        fontSize: 20,
        fontFamily: fonts.default,
        textAlign: 'center',
        marginTop: 100
    },
    variantTitle: {
        color: '#7d7d7d',
        fontSize: 18,
        fontFamily: fonts.secondary,
        margin: 10
    },
    variantItem: {
        paddingTop: 10,
        paddingLeft: 60,
        paddingRight: 20,
        paddingBottom: 10,
        borderBottomWidth: 2,
        borderBottomColor: '#ebe4e0',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    variantName: {
        color: colors.defaultText,
        fontSize: 16,
        fontFamily: fonts.secondary
    },
    variantValue: {
        color: '#7d7d7d',
        fontSize: 14,
        fontFamily: fonts.secondary
    },
    waiterCallContainer: {
        marginRight: 120,
        marginLeft: 15
    },
    // closeButton: {
    //     position: 'absolute',
    //     top: 10,
    //     left: 10,
    // },
    closeIcon: {
        color: '#BABABA',
        fontSize: 26
    },
    h2: {
        fontFamily: fonts.secondary,
        fontSize: 22,
        color: colors.defaultText,
        textAlign: 'center',
        marginBottom: 15,
        marginHorizontal: 40
    },
    comandasList: {
        flex: 1
    },
    comandasListContainer: {
        // justifyContent: 'center',
        // flexGrow: 1
    },
    productContainer: {
        flexDirection: 'row',
        borderRadius: 2
    },
    productImage: {
        width: 192,
        minHeight: 144,
        backgroundColor: colors.secondaryColor,
        resizeMode: 'contain'
    },
    productInfo: {
        backgroundColor: '#F7F2F2',
        padding: 16,
        flex: 1
    },
    productTitle: {
        color: colors.defaultText,
        fontSize: 18,
        fontFamily: fonts.secondary,
        marginBottom: 16
    },
    productDescription: {
        color: colors.defaultText,
        fontSize: 14,
        fontFamily: fonts.default,
        marginBottom: 16
    },
    productValue: {
        color: colors.defaultText,
        fontSize: 14,
        fontFamily: fonts.default,
        fontWeight: 'bold'
    },
    instructions: {
        fontFamily: fonts.default,
        fontSize: 14,
        color: colors.defaultText,
        marginTop: 16,
        marginBottom: 8
    },
    productToolbar: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        backgroundColor: '#F2F1F1',
        padding: 15,
        alignItems: 'center',
        marginBottom: 8
    },
    productTotalValue: {
        color: colors.defaultText,
        fontSize: 22,
        fontFamily: fonts.secondary,
        textAlign: 'right',
        fontWeight: 'bold'
    },
    instructions2: {
        color: colors.defaultText,
        fontSize: 24,
        fontFamily: fonts.default,
        fontWeight: 'bold',
        marginBottom: 16
    },
    variantsContainer: {
        position: 'relative',
        flex: 1
    },
    actionBar: {
        paddingTop: 16,
        borderTopWidth: 1,
        borderTopColor: colors.gray
    },
    scrollIndicator: {
        fontSize: 16,
        textAlign: 'center',
        paddingVertical: 4
    }
};
