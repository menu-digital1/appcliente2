import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import ImageLoad from 'react-native-image-placeholder';
import _ from 'lodash';
import moment from 'moment';
import Icon from 'react-native-vector-icons/FontAwesome';
import HTMLView from 'react-native-htmlview';

import { colors } from '../styles/colors';
import { fonts } from '../styles/app';

require('moment/locale/pt-br');

moment.locale('pt-br');

export default class EventItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            open: false
        };
    }

    getEventPeriod() {
        var format = 'DD MMM';

        if (this.props.event.data_inicio == this.props.event.data_fim) {
            return moment(this.props.event.data_inicio).format(format);
        }

        return `${moment(this.props.event.data_inicio).format(format)} a ${moment(this.props.event.data_fim).format(format)}`;
    }

    toggleDetails() {
        this.setState({ open: !this.state.open });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.card}>
                    <View style={styles.imageContainer}>
                        <ImageLoad source={{ uri: this.props.event.full_imagem }} isShowActivity={false} style={styles.image} />
                    </View>

                    <View style={styles.info}>
                        <View style={styles.infoContent}>
                            <Text style={styles.infoTitle}>{this.props.event.nome}</Text>

                            <Text style={styles.subtitle}>{this.getEventPeriod()}</Text>

                            <Text style={styles.infoDescription} numberOfLines={2}>
                                {this.props.event.descricao.replace(/<\/?[^>]+(>|$)/g, '')}
                            </Text>
                        </View>

                        <TouchableOpacity onPress={() => this.toggleDetails()}>
                            <View style={styles.textBtn}>
                                <Text style={styles.textBtnText}>SAIBA MAIS</Text>

                                <Icon name={this.state.open ? 'chevron-up' : 'chevron-down'} style={styles.textBtnIcon} />
                            </View>
                        </TouchableOpacity>
                    </View>
                </View>

                {this.state.open && (
                    <View style={styles.cardContent}>
                        <HTMLView value={this.props.event.descricao} stylesheet={htmlStyles} />
                    </View>
                )}
            </View>
        );
    }
}

const styles = {
    container: {
        marginBottom: 16,
        borderRadius: 2,
        backgroundColor: colors.surface
    },
    card: {
        flexDirection: 'row',
        height: 192
    },
    imageContainer: {
        width: 256,
        height: '100%',
        position: 'relative'
    },
    image: {
        height: '100%',
        width: '100%',
        resizeMode: 'contain'
    },
    price: {
        fontSize: 16,
        fontFamily: fonts.default,
        color: colors.defaultText,
        fontWeight: 'bold'
    },
    info: {
        padding: 16,
        flex: 1
    },
    infoContent: {
        flex: 1
    },
    details: {
        flex: 1
    },
    infoTitle: {
        color: colors.onSurface,
        fontSize: 20,
        fontFamily: fonts.secondary
    },
    subtitle: {
        color: colors.onSurface,
        fontSize: 16,
        fontFamily: fonts.default,
        marginBottom: 8,
        fontStyle: 'italic'
    },
    infoDescription: {
        color: colors.onSurface,
        fontSize: 14,
        fontFamily: fonts.default,
        marginBottom: 8,
        textAlign: 'justify'
    },
    infoValue: {
        color: colors.defaultText,
        fontSize: 16,
        fontFamily: fonts.default,
        marginBottom: 5,
        fontWeight: 'bold'
    },
    cardContent: {
        backgroundColor: colors.surface,
        padding: 16,
        borderTopWidth: 1,
        borderTopColor: 'rgba(0, 0, 0, 0.12)'
    },
    textBtn: {
        height: 36,
        paddingHorizontal: 8,
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        alignSelf: 'flex-end'
    },
    textBtnText: {
        textAlign: 'center',
        color: colors.tertiary,
        fontSize: 14,
        fontFamily: fonts.secondary
    },
    textBtnIcon: {
        color: colors.tertiary,
        fontSize: 11,
        marginLeft: 8
    }
};

const htmlStyles = {
    p: {
        color: colors.onSurface,
        fontFamily: fonts.default,
        fontSize: 16,
        marginBottom: 0,
        padding: 0,
        textAlign: 'justify'
    },
    li: {
        color: colors.onSurface,
        fontFamily: fonts.default,
        fontSize: 16,
        marginBottom: 8
    }
};
