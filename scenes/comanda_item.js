import React, { Component } from 'react';
import { Text, TouchableOpacity, Alert, Image, View } from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import numeral from 'numeral';
import { connect } from "react-redux";
import _ from 'lodash';

import { colors } from '../styles/colors';
import { fonts } from '../styles/app';
import { updateComandas } from '../reducers/actions';
import Api from '../services/api';

class ComandaItem extends Component {
	removeComanda() {
		Alert.alert(
			'REMOVER',
			'Tem certeza que deseja remover essa Comanda?',
			[
				{ text: 'Não' },
				{
					text: 'Sim',
					onPress: () => {
						var comandas = this.props.comandas;
    
						comandas = _.reject(comandas, c => c.number == this.props.comanda.number);
				
						this.props.updateComandas(comandas);

						var data = {
							code: this.props.comanda.number,
						};

						Api.post('unlink-comanda', data);
					}
				}
			],
			{ cancelable: false }
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<Image source={require('../img/qrcode.png')} style={styles.icon} />

				<Text style={styles.number}>{numeral(this.props.comanda.number).format('00')}</Text>

				<TouchableOpacity style={styles.removeButton} onPress={() => this.removeComanda()}>
					<Icon name='trash' style={styles.removeButtonIcon} />
				</TouchableOpacity>
			</View>
		);
	}
}

export default connect(
	state => ({
		comandas: state.comandasReducer.comandas
	}),
	{ updateComandas }
)(ComandaItem);

const styles = {
	container: {
		marginHorizontal: 32,
		backgroundColor: '#e2e7ee',
		borderWidth: 2,
		borderColor: '#d7dee2',
		borderRadius: 8,
		padding: 8,
		justifyContent: 'center',
		alignItems: 'center',
		position: 'relative',
		width: 184,
		height: 296
	},
	icon: {
		height: 72,
		resizeMode: 'contain',
		marginBottom: 8
	},
	number: {
		textAlign: 'center',
		fontFamily: fonts.secondary,
		fontSize: 68,
		color: colors.onSurface
	},
	removeButton: {
		position: 'absolute',
		top: 0,
		right: 0,
		backgroundColor: colors.primary,
		borderRadius: 48,
		width: 48,
		height: 48,
		justifyContent: 'center',
		alignItems: 'center'
	},
	removeButtonIcon: {
		fontSize: 24,
		color: colors.onPrimary
	}
};
