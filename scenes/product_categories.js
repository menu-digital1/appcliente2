import React, { Component } from 'react'
import { Text, View, FlatList } from 'react-native'

import ProductCategoriesItem from './product_categories_item';

export default class ProductCategories extends Component {
    render() {
        return (
            <View>
                <Text>{this.props.category.nome}</Text>

                <FlatList 
                    data={this.props.category.subcategoria}
                    keyExtractor={(i, key) => key.toString()}
                    renderItem={b => <ProductCategoriesItem category={b.item} findProducts={this.props.findProducts} />}
                />
            </View>
        )
    }
}
