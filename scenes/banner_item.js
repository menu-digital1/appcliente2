import React, { Component } from 'react';
import { Text, View } from 'react-native';
import ImageLoad from 'react-native-image-placeholder';
import LinearGradient from 'react-native-linear-gradient';

import Btn from '../components/btn';
import Variants from './variants';
import NoVariants from './no_variants';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';
import SmallModal from '../components/small_modal';
import LargeModal from '../components/large_modal';
import { Actions } from 'react-native-router-flux';

export default class BannerItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false
        };

        this.closeModal = this.closeModal.bind(this);
    }

    addToOrder() {
        this.setState({ modalVisible: true });
    }

    closeModal() {
        this.setState({ modalVisible: false });
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.imageContainer}>
                    <LinearGradient
                        start={{ x: 0.8, y: 0.8 }}
                        end={{ x: 1, y: 0 }}
                        colors={['rgba(0, 0, 0, 0)', 'rgba(0, 0, 0, 0.5)']}
                        style={styles.backdrop}
                    ></LinearGradient>

                    <ImageLoad source={{ uri: this.props.banner.full_imagem }} isShowActivity={false} style={styles.image} />
                </View>

                <View style={styles.descriptionContainer}>
                    <Text style={styles.description}>{this.props.banner.texto}</Text>

                    {this.props.banner.produto && this.props.hasComandas && (
                        <View>
                            <Btn text='PEÇA JÁ' type='primary' onPress={() => this.addToOrder()} />

                            {this.props.banner.produto.variantes && this.props.banner.produto.variantes.length === 0 && (
                                <SmallModal visible={this.state.modalVisible} closeModal={() => this.closeModal()}>
                                    <NoVariants product={this.props.banner.produto} closeModal={() => this.closeModal()} />
                                </SmallModal>
                            )}

                            {this.props.banner.produto.variantes && this.props.banner.produto.variantes.length !== 0 && (
                                <LargeModal visible={this.state.modalVisible} closeModal={() => this.closeModal()}>
                                    <Variants
                                        variants={this.props.banner.produto.variantes}
                                        product={this.props.banner.produto}
                                        closeModal={() => this.closeModal()}
                                    />
                                </LargeModal>
                            )}
                        </View>
                    )}

                    {this.props.banner.event && this.props.hasComandas && (
                        <Btn text='SAIBA MAIS' type='primary' onPress={() => Actions.events()} />
                    )}
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        position: 'relative'
    },
    imageContainer: {
        flex: 1,
        position: 'relative'
    },
    backdrop: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 99
    },
    image: {
        flex: 1,
        resizeMode: 'contain'
    },
    descriptionContainer: {
        backgroundColor: 'rgba(255, 255, 255, 0.85)',
        position: 'absolute',
        bottom: 0,
        left: 0,
        right: 0,
        margin: 16,
        padding: 24,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        height: 98,
        zIndex: 999
    },
    description: {
        fontFamily: fonts.default,
        color: colors.onSurface,
        fontSize: 22,
        marginRight: 24
    },
    modalContent: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        alignItems: 'center'
    },
    modalBody: {
        backgroundColor: '#fff',
        borderRadius: 5,
        flex: 1,
        alignSelf: 'stretch'
    },
    modalNoVariantBody: {
        backgroundColor: '#fff',
        borderRadius: 5
    },
    modalContentLarge: {
        padding: 40
    },
    modalContentSmall: {
        paddingHorizontal: 300,
        paddingVertical: 100
    }
};
