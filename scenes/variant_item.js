import React, { Component } from 'react';
import { Text, View } from 'react-native';
import numeral from 'numeral';
import _ from 'lodash';
import { connect } from 'react-redux';

import NumberSpinner from '../components/number_spinner';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';
import Radio from '../components/radio';
import Comanda from '../models/Comanda';
import { updateComandas } from '../reducers/actions';

export default class VariantItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            quantity: 0,
            selected: false,
            comandas: this.props.comandas
        };
    }

    // componentWillReceiveProps() {
    // 	var quantity = 0;
    // 	var selected = false;

    // 	if (this.props.productQuantity <= 0) {
    // 		quantity = 0;
    // 	} else {
    // 		if (this.props.currentComanda.products) {
    // 			var product = _.find(this.props.currentComanda.products, p => p.id == this.props.product._id);

    // 			if (product) {
    // 				if (product.variants) {
    // 					for (var v of product.variants) {
    // 						if (v.quantity) {
    // 							if (v.id == this.props.item._id) {
    // 								quantity = v.quantity;
    // 							}
    // 						} else {
    // 							if (v.selected) {
    // 								selected = true;
    // 							}
    // 						}
    // 					}
    //                 }
    // 			}
    // 		}
    // 	}

    //     this.setState({ quantity, selected });
    // }

    setVariantQuantity(value) {
        var data = {
            id: this.props.item._id,
            name: this.props.item.nome,
            value: this.props.item.valor_adicional,
            quantity: value
        };

        var comandas = this.state.comandas;

        var productIndex = _.findIndex(comandas[this.props.currentComanda].products, p => p.id == this.props.product._id);

        if (!comandas[this.props.currentComanda].products[productIndex].variants) {
            comandas[this.props.currentComanda].products[productIndex].variants = [];
        }

        var variantIndex = _.findIndex(
            comandas[this.props.currentComanda].products[productIndex].variants,
            v => v.id == this.props.item._id
        );

        if (variantIndex == -1) {
            comandas[this.props.currentComanda].products[productIndex].variants.push(data);
        } else {
            if (value <= 0) {
                comandas[this.props.currentComanda].products[productIndex].variants.splice(variantIndex, 1);
            } else {
                comandas[this.props.currentComanda].products[productIndex].variants[variantIndex] = {
                    id: data.id,
                    name: data.name,
                    value: data.value,
                    quantity: data.quantity
                };
            }
        }

        this.props.setComandas(comandas);
    }

    setVariantChoice() {
        var comandas = this.state.comandas;

        var productIndex = _.findIndex(comandas[this.props.currentComanda].products, p => p.id == this.props.product._id);

        if (!comandas[this.props.currentComanda].products[productIndex].variants) {
            comandas[this.props.currentComanda].products[productIndex].variants = [];
        }

        for (var i of this.props.variant.itens_variante) {
            var variantIndex = _.findIndex(comandas[this.props.currentComanda].products[productIndex].variants, v => v.id == i._id);

            if (variantIndex == -1) {
                if (this.props.item._id == i._id) {
                    comandas[this.props.currentComanda].products[productIndex].variants.push({
                        id: i._id,
                        name: i.nome,
                        value: i.valor_adicional,
                        selected: true
                    });
                } else {
                    comandas[this.props.currentComanda].products[productIndex].variants.push({
                        id: i._id,
                        name: i.nome,
                        value: i.valor_adicional,
                        selected: false
                    });
                }
            } else {
                comandas[this.props.currentComanda].products[productIndex].variants[variantIndex].id = i._id;
                comandas[this.props.currentComanda].products[productIndex].variants[variantIndex].name = i.nome;
                comandas[this.props.currentComanda].products[productIndex].variants[variantIndex].value = i.valor_adicional;

                if (this.props.item._id == i._id) {
                    if (comandas[this.props.currentComanda].products[productIndex].variants[variantIndex].selected == true) {
                        comandas[this.props.currentComanda].products[productIndex].variants[variantIndex].selected = false;
                    } else {
                        comandas[this.props.currentComanda].products[productIndex].variants[variantIndex].selected = true;
                    }
                } else {
                    comandas[this.props.currentComanda].products[productIndex].variants[variantIndex].selected = false;
                }
            }
        }

        this.props.setComandas(comandas);

        console.log(comandas);

        // if (this.props.selected == this.props.item._id) {
        // 	this.props.setSelected(null);
        // } else {
        // 	this.props.setSelected(this.props.item._id);
        // }

        // Comanda.updateComanda(currentComanda);
    }

    getQuantity() {
        var quantity = 0;

        if (this.state.comandas[this.props.currentComanda].products) {
            var product = _.find(this.state.comandas[this.props.currentComanda].products, p => p.id == this.props.product._id);

            if (product && product.variants) {
                for (var v of product.variants) {
                    if (v.id === this.props.item._id && v.quantity) {
                        quantity = v.quantity;
                    }
                }
            }
        }

        return quantity;
    }

    getSelected() {
        if (this.state.comandas[this.props.currentComanda].products) {
            var product = _.find(this.state.comandas[this.props.currentComanda].products, p => p.id == this.props.product._id);

            if (product && product.variants) {
                for (var v of product.variants) {
                    if (v.id === this.props.item._id && v.selected == true) {
                        return true;
                    }
                }
            }
        }

        return false;
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.info}>
                    <Text style={styles.name}>{this.props.item.nome}</Text>

                    <Text style={styles.price}>{`+${numeral(this.props.item.valor_adicional).format('0,0.00')}`}</Text>
                </View>

                {this.props.variant.tipo == 'true' && (
                    <NumberSpinner
                        onChange={value => this.setVariantQuantity(value)}
                        value={this.getQuantity()}
                        min={0}
                        max={this.props.variant.maxSelect}
                    />
                )}

                {this.props.variant.tipo == 'false' && (
                    <Radio value={this.getSelected()} color={colors.secondaryColor} onPress={() => this.setVariantChoice()} />
                )}
            </View>
        );
    }
}

const styles = {
    container: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 16,
	},
	info: {
		flexDirection: 'row',
		alignItems: 'baseline',
	},
    name: {
        color: colors.defaultText,
        fontSize: 16,
        fontFamily: fonts.default,
		fontWeight: 'bold',
		marginRight: 16,
    },
    title: {
        color: colors.defaultText,
        fontSize: 16,
        fontFamily: fonts.default
	},
};
