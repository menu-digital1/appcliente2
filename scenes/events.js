import React, { Component } from 'react';
import { Text, View, FlatList, Image } from 'react-native';
import { Actions } from 'react-native-router-flux';

import { app, fonts } from '../styles/app';
import OrdersBar from '../components/order_bar';
import MenuButton from '../components/menu_button';
import Api from '../services/api';
import EventItem from './event_item';
import DonutLoading from '../components/donut_loading';
import InternetMonitor from './internet_monitor';
import TableBar from '../components/table_bar';

export default class Events extends Component {
    constructor(props) {
        super(props);

        this.state = {
            active: 'next',
            events: [],
            loading: true
        };

        this.activeText = {
            next: 'Próximos',
            today: 'Hoje',
            week: 'Essa semana',
            month: 'Esse mês'
        };
    }

    componentDidMount() {
        this.fetchEvents();
    }

    back() {
        Actions.reset('home');
    }

    setActive(active) {
        this.setState({ active }, () => {
            this.fetchEvents();
        });
    }

    fetchEvents() {
        this.setState({ loading: true });

        Api.post('events', { filter: this.state.active }).then(events => {
            this.setState({ events, loading: false });
        });
    }

    render() {
        return (
            <View style={app.mainContainer}>
                <InternetMonitor />

                <View style={app.menuContainer}>
                    <TableBar onBack={() => this.back()} />

                    <View style={app.menuContainerInner}>
                        <MenuButton text='Próximos' onPress={() => this.setActive('next')} active={this.state.active == 'next'} />
                        <MenuButton text='Hoje' onPress={() => this.setActive('today')} active={this.state.active == 'today'} />
                        <MenuButton text='Essa semana' onPress={() => this.setActive('week')} active={this.state.active == 'week'} />
                        <MenuButton text='Esse mês' onPress={() => this.setActive('month')} active={this.state.active == 'month'} />
                    </View>
                </View>

                <View style={app.mainContent}>
                    <View style={app.mainHeader}>
                        <View style={styles.titleContainer}>
                            <Text style={app.h2}>EVENTOS</Text>

                            <Text style={styles.subtitle}>{this.activeText[this.state.active].toUpperCase()}</Text>
                        </View>

                        <OrdersBar mini />
                    </View>

                    {this.state.loading && <DonutLoading />}

                    {!this.state.loading && this.state.events.length !== 0 && (
                        <FlatList
                            data={this.state.events}
                            keyExtractor={(i, key) => key.toString()}
                            renderItem={e => <EventItem event={e.item} />}
                        />
                    )}

                    {!this.state.loading && this.state.events.length === 0 && (
                        <View style={styles.emptyDataContainer}>
                            <Image source={require('../img/calendar.png')} style={styles.emptyDataImage} />

                            <Text style={styles.emptyDataText}>Nenhum evento encontrado</Text>
                        </View>
                    )}
                </View>
            </View>
        );
    }
}

const styles = {
    innerContent: {
        flex: 1,
        paddingHorizontal: 40,
        paddingTop: 120,
        paddingBottom: 40
    },
    menuInner: {
        flex: 1,
        justifyContent: 'center'
    },
    titleContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    subtitle: {
        fontFamily: fonts.secondary,
        fontSize: 24,
        marginLeft: 16,
        color: 'rgba(42, 42, 42, 0.6)'
    },
    emptyDataContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    emptyDataImage: {
        height: 120,
        marginBottom: 16,
        resizeMode: 'contain'
    },
    emptyDataText: {
        textAlign: 'center',
        fontSize: 24,
        fontFamily: fonts.secondary,
        color: '#bababa'
    }
};
