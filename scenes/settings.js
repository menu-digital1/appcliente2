import React, { Component } from 'react';
import { Text, View, Alert, Image, BackHandler, TouchableOpacity } from 'react-native';
import numeral from 'numeral';
import Icon from 'react-native-vector-icons/FontAwesome';
import { Actions } from 'react-native-router-flux';
import CardView from 'react-native-cardview';

import Btn from '../components/btn';
import Comanda from '../models/Comanda';
import Route from '../models/Route';
import Table from '../models/Table';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';

export default class Settings extends Component {
	constructor(props) {
		super(props);

		this.state = {
			table: null
		};
	}

	componentWillMount() {
		Table.getTable().then(table => this.setState({ table }));
	}

	clearComandas() {
		Alert.alert(
			'CONFIRMAR',
			'Tem certeza que deseja remover todas as Comandas?',
			[
				{ text: 'Não' },
				{
					text: 'Sim',
					onPress: () => {
						Table.getTable().then(table => {
							Comanda.clearComandas(table).then(() => {
								Route.homeRoute();
							});
						});
					}
				},
			],
			{ cancelable: false }
		);
	}

	clearTable() {
		Alert.alert(
			'CONFIRMAR',
			'Tem certeza que deseja mudar a Mesa?',
			[
				{ text: 'Não' },
				{
					text: 'Sim',
					onPress: () => {
						Table.getTable().then(table => {
							Table.clearTable().then(() => {
								Comanda.clearComandas(table).then(() => {
									Route.homeRoute();
								});
							});
						});
					}
				},
			],
			{ cancelable: false }
		);
	}

	exitApp() {
		Alert.alert(
			'CONFIRMAR',
			'Tem certeza que deseja sair do aplicativo?',
			[
				{ text: 'Não' },
				{
					text: 'Sim',
					onPress: () => {
						BackHandler.exitApp();
					}
				}
			],
			{ cancelable: false }
		);
	}

	back() {
		Actions.reset('home');
	}

	render() {
		return (
			<View style={styles.container}>
				<View style={styles.header}>
					<TouchableOpacity
						onPress={() => this.back()}
						hitSlop={{
							top: 40,
							left: 40,
							bottom: 40,
							right: 40
						}}
					>
						<Icon name='arrow-left' style={styles.backIcon} />
					</TouchableOpacity>

					<Text style={styles.h1}>CONFIGURAÇÕES</Text>
				</View>

				<View style={styles.innerContainer}>
					<CardView cardElevation={2} style={styles.settingItem}>
						<View style={styles.settingItemInner}>
							<Image source={require('../img/qrcode.png')} style={styles.settingIcon} />

							<View style={styles.info}>
								<Text style={styles.settingDescription}>Remove todas as comandas da memória do tablet.</Text>

								<Text style={styles.settingDescription}>Isso não exclui os pedidos associados às comandas.</Text>
							</View>

							<View style={styles.settingActions}>
								<Btn text='REMOVER COMANDAS' onPress={() => this.clearComandas()} />
							</View>
						</View>
					</CardView>

					<CardView cardElevation={2} style={styles.settingItem}>
						<View style={styles.settingItemInner}>
							<Image source={require('../img/table.png')} style={styles.settingIcon} />

							<View style={styles.info}>
								<Text style={styles.settingDescription}>Mudar o número da mesa atualmente associada a esse tablet.</Text>

								<Text style={styles.settingDescription}>Número da mesa atual:</Text>

								<Text style={styles.tableNumber}>{numeral(this.state.table).format('00')}</Text>
							</View>

							<View style={styles.settingActions}>
								<Btn text='MUDAR MESA' onPress={() => this.clearTable()} />
							</View>
						</View>
					</CardView>

					<CardView cardElevation={2} style={styles.settingItem}>
						<View style={styles.settingItemInner}>
							<Image source={require('../img/exit.png')} style={styles.settingIcon} />

							<View style={styles.info}>
								<Text style={styles.settingDescription}>Sair do aplicativo e retornar à tela inicial do Tablet.</Text>
							</View>

							<View style={styles.settingActions}>
								<Btn text='SAIR' onPress={() => this.exitApp()} />
							</View>
						</View>
					</CardView>
				</View>
			</View>
		);
	}
}

const styles = {
	container: {
		flex: 1,
		padding: 40,
		backgroundColor: colors.surface
	},
	header: {
		flexDirection: 'row',
		marginBottom: 32,
		alignItems: 'center'
	},
	backIcon: {
		fontSize: 24,
		marginRight: 16,
		color: colors.defaultText
	},
	h1: {
		fontFamily: fonts.secondary,
		color: colors.defaultText,
		fontSize: 32
	},
	innerContainer: {
		flex: 1,
		flexDirection: 'row',
		marginHorizontal: -40
	},
	settingItem: {
		marginHorizontal: 40,
		flex: 1,
		paddingHorizontal: 40,
		paddingVertical: 144
	},
	settingItemInner: {
		height: '100%',
		justifyContent: 'space-between'
	},
	settingIcon: {
		height: 88,
		resizeMode: 'contain',
		alignSelf: 'center'
	},
	settingDescription: {
		fontFamily: fonts.secondary,
		color: colors.defaultText,
		fontSize: 18,
		textAlign: 'center',
		marginBottom: 16
	},
	tableNumber: {
		fontFamily: fonts.secondary,
		color: colors.defaultText,
		fontSize: 28,
		textAlign: 'center'
	},
	settingActions: {}
};
