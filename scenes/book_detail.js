import React, { Component } from 'react'
import { Text, View } from 'react-native';
import ImageLoad from 'react-native-image-placeholder';

import { colors } from '../styles/colors';
import { fonts } from '../styles/app';

export default class BookDetail extends Component {
    render() {
        return (
           <View style={styles.container}>
               <ImageLoad
                    source={{ uri: this.props.book.full_capa }}
                    isShowActivity={false}
                    style={styles.image}
                />

                <View style={styles.infoContainer}>
                    <Text style={styles.title}>{this.props.book.titulo}</Text>

                    <Text style={styles.author}>{this.props.book.autor}</Text>

                    <Text style={styles.description}>{this.props.book.resumo}</Text>
                </View>
           </View>
        )
    }
}

const styles = {
    container: {
        flexDirection: 'row',
    },
    image: {
        height: 350,
        width: '35%',
        resizeMode: 'cover',
        marginRight: 15,
    },
    infoContainer: {
        flex: 1,
    },
    title: {
        fontFamily: fonts.secondary,
        color: colors.defaultText,
        fontSize: 20,
        marginBottom: 10,
    },
    author: {
        fontFamily: fonts.default,
        color: colors.defaultText,
        fontSize: 16,
        fontWeight: 'bold',
        fontStyle: 'italic',
        marginBottom: 10,
    },
    description: {
        fontFamily: fonts.default,
        color: colors.defaultText,
        fontSize: 16,
        lineHeight: 18,
        textAlign: 'justify',
    },
}