import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import numeral from 'numeral';
import { connect } from 'react-redux';

import { colors } from '../styles/colors';
import { fonts } from '../styles/app';

export default class VariantsComandaItem extends Component {
	render() {
		return (
			<View>
				{this.props.comandas[this.props.currentComanda].number !== this.props.comanda.number && (
					<TouchableOpacity onPress={() => this.props.setCurrentComanda(this.props.index)}>
						<Text style={styles.comandaButton}>{`COMANDA ${numeral(this.props.comanda.number).format('00')}`}</Text>
					</TouchableOpacity>
				)}

				{this.props.comandas[this.props.currentComanda].number === this.props.comanda.number && (
					<Text style={styles.activeComandaButton}>{`COMANDA ${numeral(this.props.comanda.number).format('00')}`}</Text>
				)}
			</View>
		);
	}
}

const styles = {
	comandaButton: {
		// backgroundColor: colors.secondaryColor,
		color: colors.secondary,
		textAlign: 'center',
		fontFamily: fonts.default,
		fontSize: 20,
		padding: 15,
		marginBottom: 15,
		marginHorizontal: 40,
		fontWeight: 'bold'
	},
	activeComandaButton: {
		backgroundColor: '#fff',
		color: colors.secondaryColor,
		textAlign: 'center',
		fontFamily: fonts.default,
		fontSize: 22,
		padding: 15,
		paddingRight: 60,
		marginBottom: 15,
		fontWeight: 'bold',
		marginLeft: 40
	}
};
