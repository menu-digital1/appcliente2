import React, { Component } from 'react'

import Btn from '../components/btn';

export default class ProductCategoriesItem extends Component {
    render() {
        return (
            <Btn text={this.props.category.nome} onPress={() => this.props.findProducts(this.props.category['_id']['$oid'])} />
        )
    }
}
