import React, { Component } from "react";
import { FlatList, View, Text, Image, ScrollView } from "react-native";
import Accordion from "react-native-collapsible/Accordion";
import _ from "lodash";
import Icon from "react-native-vector-icons/FontAwesome";
import { connect } from "react-redux";

import Product from "../models/Product";
import ProductItem from "./product_item";
import OrdersBar from "../components/order_bar";
import { colors } from "../styles/colors";
import { fonts, app } from "../styles/app";
import DonutLoading from "../components/donut_loading";
import { Actions } from "react-native-router-flux";
import InternetMonitor from "./internet_monitor";
import TableBar from "../components/table_bar";
import Btn from "../components/btn";
import LargeModal from "../components/large_modal";
import Order from "./order";
import { updateComandas } from '../reducers/actions';

class Products extends Component {
    constructor(props) {
        super(props);

        this.state = {
            categories: [],
            products: [],
            activeSections: [0],
            currentSubcategory: null,
            loadingCategories: true,
            loadingProducts: true,
            modalVisible: false
        };

        this.findProducts = this.findProducts.bind(this);
        this.closeModal = this.closeModal.bind(this);
    }

    componentDidMount() {
        this.setState({
            loadingCategories: true,
            loadingProducts: true
        });

        this.fetchCategories().then(() => {
            if (this.state.categories.length !== 0) {
                this.setState({
                    currentSubcategory: this.state.categories[0].subcategoria[0]
                });
                this.fetchProducts(this.state.categories[0].subcategoria[0]._id);
            } else {
                this.setState({
                    loadingCategories: false,
                    loadingProducts: false
                });
            }

            this.setState({ loadingCategories: false });
        });
    }

    fetchCategories() {
        return new Promise((resolve, reject) => {
            Product.findCategories().then(categories => {
                this.setState({ categories });

                resolve(true);
            });
        });
    }

    fetchProducts(subcategoryId) {
        this.setState({ loadingProducts: true });

        Product.findProducts(subcategoryId).then(products => {
            this.setState({
                products,
                loadingProducts: false
            });
        });
    }

    findProducts(subcategory) {
        this.setState({ currentSubcategory: subcategory });
        this.fetchProducts(subcategory._id);
    }

    _renderHeader = section => {
        return (
            <View style={styles.categoriesHeader}>
                <Text style={styles.headerText}>{section.nome}</Text>

                <Icon
                    name={
                        this.state.categories[this.state.activeSections[0]] &&
                        this.state.categories[this.state.activeSections[0]].id === section.id
                            ? "chevron-up"
                            : "chevron-down"
                    }
                    style={styles.headerIcon}
                />
            </View>
        );
    };

    _renderContent = section => {
        return (
            <FlatList
                extraData={this.state}
                style={styles.subcategoriesList}
                data={section.subcategoria}
                keyExtractor={(i, key) => key.toString()}
                renderItem={i => (
                    <Text
                        style={[
                            styles.subcategoryItem,
                            this.state.currentSubcategory._id == i.item._id ? styles.subcategoryActive : ""
                        ]}
                        onPress={() => this.findProducts(i.item)}
                    >
                        {i.item.nome}
                    </Text>
                )}
            />
        );
    };

    _updateSections = activeSections => {
        this.setState({ activeSections });
    };

    back() {
        Actions.reset("home");
    }

    hasOrders() {
        var empty = true;

        for (var c of this.props.comandas) {
            if (c.products && c.products.length !== 0) {
                empty = false;
            }
        }

        return !empty;
    }

    showOrders() {
        this.setState({ modalVisible: true });
    }

    closeModal() {
        this.setState({ modalVisible: false });
    }

    render() {
        return (
            <View style={app.mainContainer}>
                <InternetMonitor />

                <View style={app.menuContainer}>
                    <TableBar onBack={() => this.back()} />

                    <View style={app.menuContainerInner}>
                        {!this.state.loadingCategories && (
                            <ScrollView contentContainerStyle={styles.accordion}>
                                <Accordion
                                    activeSections={this.state.activeSections}
                                    sections={this.state.categories}
                                    renderHeader={this._renderHeader}
                                    renderContent={this._renderContent}
                                    onChange={this._updateSections}
                                />
                            </ScrollView>
                        )}
                    </View>
                </View>

                <View style={app.mainContent}>
                    <View style={app.mainHeader}>
                        {!this.state.loadingProducts && this.state.products.length !== 0 && (
                            <Text style={app.h2}>{this.state.currentSubcategory.nome}</Text>
                        )}

                        <OrdersBar mini />
                    </View>

                    {this.state.loadingProducts && <DonutLoading />}

                    {!this.state.loadingProducts && this.state.products.length !== 0 && (
                        <FlatList
                            data={this.state.products}
                            style={styles.productsList}
                            keyExtractor={(i, key) => key.toString()}
                            renderItem={b => <ProductItem product={b.item} />}
                        />
                    )}

                    {!this.state.loadingProducts && this.state.products.length === 0 && (
                        <View style={styles.emptyDataContainer}>
                            <Image source={require("../img/coffee-cup.png")} style={styles.emptyDataImage} />

                            <Text style={styles.emptyDataText}>Nenhum produto encontrado</Text>
                        </View>
                    )}

                    {this.hasOrders() && (
                        <View style={styles.ordersContainer}>
                            <Btn text="Enviar Pedido" onPress={() => this.showOrders()} />
                        </View>
                    )}
                </View>

                <LargeModal visible={this.state.modalVisible} closeModal={this.closeModal}>
                    <Order closeModal={this.closeModal} />
                </LargeModal>
            </View>
        );
    }
}

export default connect(
    state => ({ comandas: state.comandasReducer.comandas }),
    {updateComandas}
)(Products);

const styles = {
    container: {
        flex: 1,
        flexDirection: "row"
    },
    menuContainer: {
        height: "100%",
        backgroundColor: colors.primaryColor,
        paddingHorizontal: 40,
        paddingBottom: 40,
        paddingTop: 120,
        width: 400
    },
    content: {
        flex: 1
    },
    logo: {
        height: 250,
        resizeMode: "contain",
        alignSelf: "center"
    },
    categoriesHeader: {
        backgroundColor: "#fff",
        padding: 16,
        borderTopLeftRadius: 2,
        borderTopRightRadius: 2,
        alignItems: "center",
        flexDirection: "row",
        marginTop: 24,
        height: 56
    },
    headerText: {
        textAlign: "center",
        color: colors.defaultText,
        fontSize: 20,
        fontFamily: fonts.secondary,
        flex: 1
    },
    headerIcon: {
        textAlign: "center",
        color: colors.defaultText,
        fontSize: 16
    },
    subcategoriesList: {
        backgroundColor: "#f1ebe7",
        borderBottomLeftRadius: 2,
        borderBottomRightRadius: 2
    },
    subcategoryItem: {
        color: colors.defaultText,
        fontSize: 18,
        fontFamily: fonts.secondary,
        padding: 16,
        height: 56
    },
    subcategoryActive: {
        backgroundColor: colors.secondaryColor
    },
    contentInner: {
        flex: 1,
        paddingHorizontal: 40,
        paddingTop: 120,
        paddingBottom: 40
    },
    h2: {
        color: colors.defaultText,
        fontSize: 30,
        fontFamily: fonts.secondary,
        marginBottom: 15
    },
    productsList: {},
    wrapper: {
        flex: 1
    },
    emptyDataContainer: {
        flex: 1,
        alignItems: "center",
        justifyContent: "center"
    },
    emptyDataImage: {
        height: 24,
        marginBottom: 15,
        resizeMode: "contain"
    },
    emptyDataText: {
        textAlign: "center",
        fontSize: 16,
        fontFamily: fonts.secondary,
        color: "#bababa"
    },
    backButton: {
        position: "absolute",
        top: 20,
        left: 20
    },
    backIcon: {
        color: colors.textInPrimary,
        fontSize: 32
    },
    accordion: {
        flexGrow: 1,
        justifyContent: "center"
    },
    ordersContainer: {
        backgroundColor: colors.surface,
        position: "absolute",
        bottom: 0,
        left: 0,
        right: 0,
        padding: 16,
        borderTopWidth: 1,
        borderTopColor: colors.gray,
        justifyContent: "center",
        alignItems: "center"
    }
};
