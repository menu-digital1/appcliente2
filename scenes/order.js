import React, { Component } from "react";
import { Text, View, FlatList, TextInput, Alert, ScrollView, TouchableOpacity } from "react-native";
import { connect } from "react-redux";
import _ from "lodash";
import { Actions } from "react-native-router-flux";
import Icon from "react-native-vector-icons/FontAwesome";
import numeral from "numeral";

import Comanda from "../models/Comanda";
import Btn from "../components/btn";
import Table from "../models/Table";
import OrderModel from "../models/Order";
import { updateComandas } from "../reducers/actions";
import WaiterCall from "../components/waiter_call";
import { colors } from "../styles/colors";
import { fonts } from "../styles/app";
import Route from "../models/Route";

class Order extends Component {
    constructor(props) {
        super(props);

        this.state = {
            comandas: {},
            observation: [],
            loading: false
        };
    }

    componentDidMount() {
        var observation = [];
        var comandas = Comanda.getComandas();

        this.setState({ comandas: comandas }, () => {
            for (var c of this.state.comandas) {
                if (c.products) {
                    for (var p of c.products) {
                        observation[p.id] = p.observation;
                    }
                }
            }

            this.setState({ observation });
        });
    }

    getProductFinalValue(product) {
        var total = 0;

        if (product.variants) {
            for (var v of product.variants) {
                if (v.quantity) {
                    total += v.value * v.quantity;
                } else {
                    if (v.selected) {
                        total += v.value;
                    }
                }
            }
        }

        total += product.quantity * product.value;

        return total;
    }

    setObservation(text, product, comanda) {
        var observation = this.state.observation;

        observation[`${comanda.number}_${product.id}`] = text;

        this.setState({ observation });

        var productIndex = _.findIndex(comanda.products, p => p.id == product.id);

        comanda.products[productIndex].observation = text;

        Comanda.updateComanda(comanda);
    }

    getComandaTotal(comanda) {
        var total = 0;

        for (var p of comanda.products) {
            total += this.getProductFinalValue(p);
        }

        return total;
    }

    deleteProduct(product, comanda) {
        Alert.alert(
            "CONFIRMAR",
            "Tem certeza que deseja excluir esse item?",
            [
                { text: "Não" },
                {
                    text: "Sim",
                    onPress: () => {
                        var comandas = this.state.comandas;

                        var productIndex = _.findIndex(comanda.products, p => p.id == product.id);

                        comanda.products.splice(productIndex, 1);

                        var comandaIndex = _.findIndex(comandas, c => c.number == comanda.number);

                        comandas[comandaIndex] = comanda;

                        this.setState({ comandas });

                        Comanda.updateComanda(comanda);

                        var empty = true;
                        for (var c of this.state.comandas) {
                            if (c.products.length !== 0) {
                                empty = false;
                            }
                        }

                        if (empty) {
                            this.props.closeModal();
                        }
                    }
                },
            ],
            { cancelable: false }
        );
    }

    finalize() {
        Alert.alert(
            "CONFIRMAR",
            "Tem certeza que deseja enviar o pedido?",
            [
                { text: "Não" },
                {
                    text: "Sim",
                    onPress: () => {
                        this.setState({ loading: true });

                        Table.getTable().then(table => {
                            var data = {
                                table,
                                order: this.state.comandas
                            };

                            OrderModel.sendOrder(data).then(
                                () => {
                                    var comandas = this.state.comandas;
                                    for (var index in comandas) {
                                        comandas[index].products = [];
                                    }

                                    this.setState({ comandas }, () => {
                                        Comanda.setComandas(this.state.comandas);

                                        this.props.closeModal();

                                        Actions.reset("successOrder");
                                    });
                                },
                                error => {
                                    switch (error.error_code) {
                                        case "invalid_table":
                                            Alert.alert(
                                                "ERRO",
                                                "Mesa inválida. Não será possível realizar pedidos. Por favor, chame o garçom.",
                                                [
                                                    {
                                                        text: "OK",
                                                        onPress: () => {
                                                            this.props.closeModal();

                                                            Table.clearTable().then(() => {
                                                                Comanda.clearComandas(table).then(() => {
                                                                    Actions.reset('home');
                                                                });
                                                            });
                                                        }
                                                    }
                                                ],
                                                { cancelable: false }
                                            );

                                            break;

                                        case "invalid_comandas":
                                            Alert.alert(
                                                "ERRO",
                                                "As seguintes comandas são inválidas:\n" +
                                                    error.invalid_comandas.join(", ") +
                                                    "\nNão será possível realizar pedidos com essas comandas, por favor, chame o garçom.",
                                                [
                                                    {
                                                        text: "OK",
                                                        onPress: () => {
                                                            var comandas = this.state.comandas;

                                                            for (var i of error.invalid_comandas) {
                                                                comandas = _.reject(
                                                                    comandas,
                                                                    c => c.number == Number(i)
                                                                );
                                                            }

                                                            Comanda.setComandas(comandas);

                                                            this.props.closeModal();

                                                            Actions.reset('home');
                                                        }
                                                    }
                                                ],
                                                { cancelable: false }
                                            );

                                            break;

                                        default:
                                            Alert.alert("ERRO", "Erro desconhecido", [{ text: "OK" }], {
                                                cancelable: false
                                            });
                                    }
                                }
                            );
                        });
                    }
                },
            ],
            { cancelable: false }
        );
    }

    back() {
        Actions.reset("products");
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.header}>
                    <View style={styles.titles}>
                        <Text style={styles.h1}>CONFERIR MEU PEDIDO</Text>

                        <Text style={styles.h2}>
                            Por favor, confira seu pedido antes de enviá-lo à cozinha.
                        </Text>
                    </View>
                </View>

                <FlatList
                    extraData={this.state}
                    data={this.state.comandas}
                    style={styles.list}
                    keyExtractor={(i, key) => key.toString()}
                    renderItem={c => (
                        <View>
                            <View style={styles.comandaHeader}>
                                <View style={styles.comandaContainer}>
                                    <Icon name="circle" style={styles.comandaMarker} />
                                    <Text style={styles.comandaNumber}>
                                        COMANDA {numeral(c.item.number).format("00")}
                                    </Text>
                                </View>

                                <Text style={styles.comandaTotal}>
                                    Total: {numeral(this.getComandaTotal(c.item)).format("$ 0,0.00")}
                                </Text>
                            </View>

                            <FlatList
                                extraData={this.state}
                                data={c.item.products}
                                style={styles.productsList}
                                keyExtractor={(i, key) => key.toString()}
                                renderItem={p => (
                                    <View style={styles.productsTable}>
                                        <Text style={styles.productQuantity}>{p.item.quantity}</Text>
                                        <View style={styles.productName}>
                                            <Text style={styles.productNameText}>{p.item.name}</Text>

                                            {p.item.variants && (
                                                <FlatList
                                                    extraData={this.state}
                                                    data={p.item.variants}
                                                    keyExtractor={(i, key) => key.toString()}
                                                    renderItem={v => (
                                                        <View style={styles.variantsList}>
                                                            {v.item.quantity && (
                                                                <Text style={styles.variantItem}>
                                                                    {v.item.quantity} {v.item.name}
                                                                </Text>
                                                            )}

                                                            {!v.item.quantity && v.item.selected && (
                                                                <Text style={styles.variantItem}>
                                                                    {v.item.name}
                                                                </Text>
                                                            )}
                                                        </View>
                                                    )}
                                                />
                                            )}
                                        </View>
                                        <Text style={styles.productValue}>
                                            {numeral(this.getProductFinalValue(p.item)).format("0,0.00")}
                                        </Text>

                                        <TextInput
                                            placeholder="Alguma observação sobre o seu pedido?"
                                            placeholderTextColor="#848383"
                                            style={styles.productObs}
                                            onChangeText={text => this.setObservation(text, p.item, c.item)}
                                            value={this.state.observation[`${c.item.number}_${p.item.id}`]}
                                        />

                                        <TouchableOpacity onPress={() => this.deleteProduct(p.item, c.item)}>
                                            <View style={styles.productDelete}>
                                                <Icon name="trash" style={styles.productDeleteIcon} />
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                )}
                            />
                        </View>
                    )}
                />

                <View style={styles.actions}>
                    <Btn text="VOLTAR AO CARDÁPIO" type="secondary" onPress={() => this.back()} />

                    <Btn
                        text="FINALIZAR PEDIDO"
                        loading={this.state.loading}
                        onPress={() => this.finalize()}
                    />
                </View>
            </View>
        );
    }
}

export default connect(
    state => ({
        comandas: state.comandasReducer.comandas
    }),
    {
        updateComandas
    }
)(Order);

const styles = {
    container: {
        flex: 1
    },
    header: {
        flexDirection: "row",
        justifyContent: "space-between",
        marginBottom: 24
    },
    h1: {
        fontSize: 30,
        fontFamily: fonts.secondary,
        color: colors.defaultText,
        marginBottom: 5
    },
    h2: {
        fontSize: 18,
        fontFamily: fonts.default,
        color: colors.defaultText
    },
    comandaHeader: {
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "space-between",
        marginBottom: 5
    },
    comandaContainer: {
        flexDirection: "row",
        alignItems: "center"
    },
    comandaMarker: {
        color: colors.tertiaryColor,
        fontSize: 16,
        marginRight: 15
    },
    comandaNumber: {
        fontSize: 16,
        fontFamily: fonts.secondary,
        color: colors.tertiaryColor
    },
    productsList: {
        marginLeft: 35,
        flex: 1,
        marginBottom: 30
    },
    productsTable: {
        flexDirection: "row",
        alignItems: "stretch"
    },
    productQuantity: {
        flex: 5,
        padding: 15,
        textAlign: "center",
        fontSize: 16,
        fontFamily: fonts.default,
        color: colors.defaultText,
        borderWidth: 1,
        borderColor: "#dfdbd8"
    },
    productName: {
        flex: 45,
        padding: 15,
        borderWidth: 1,
        borderColor: "#dfdbd8"
    },
    productNameText: {
        fontSize: 16,
        fontFamily: fonts.default,
        color: colors.defaultText
    },
    productValue: {
        flex: 10,
        padding: 15,
        fontSize: 16,
        fontFamily: fonts.default,
        color: colors.defaultText,
        borderWidth: 1,
        borderColor: "#dfdbd8",
        textAlign: "center"
    },
    productObs: {
        flex: 30,
        padding: 15,
        fontSize: 16,
        margin: 0,
        fontFamily: fonts.default,
        color: colors.defaultText,
        borderWidth: 1,
        borderColor: "#dfdbd8"
    },
    productDelete: {
        flex: 10,
        padding: 15,
        borderWidth: 1,
        borderColor: "#dfdbd8",
        textAlign: "center"
    },
    productDeleteIcon: {
        fontSize: 22,
        color: colors.primaryColor
    },
    comandaTotal: {
        fontSize: 16,
        fontFamily: fonts.secondary,
        color: colors.defaultText,
        textAlign: "right",
        fontWeight: "bold"
    },
    variantsList: {
        marginLeft: 20
    },
    variantItem: {
        fontSize: 12,
        fontFamily: fonts.default,
        color: colors.defaultText,
        marginVertical: 5
    },
    list: {
        flex: 1
    },
    actions: {
        flexDirection: "row",
        justifyContent: "space-between"
    }
};
