import React, { Component } from 'react';
import { Text, View, TouchableOpacity } from 'react-native';
import ImageLoad from 'react-native-image-placeholder';
import { connect } from 'react-redux';
import _ from 'lodash';
import numeral from 'numeral';
import locales from 'numeral/locales';

import Btn from '../components/btn';
import Variants from './variants';
import NoVariants from './no_variants';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';
import SmallModal from '../components/small_modal';
import LargeModal from '../components/large_modal';

numeral.locale('pt-br');

class ProductItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false
        };

        this.closeModal = this.closeModal.bind(this);
    }

    addProduct() {
        this.setState({ modalVisible: true });
    }

    closeModal() {
        this.setState({ modalVisible: false });
    }

    hasOrders() {
        var count = 0;

        for (var c of this.props.comandas) {
            if (c.products) {
                var product = _.find(c.products, p => p.id == this.props.product._id);

                if (product) {
                    count += product.quantity;
                }
            }
        }

        return count;
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.imageContainer}>
                    <TouchableOpacity onPress={() => this.addProduct()}>
                        <ImageLoad source={{ uri: this.props.product.full_imagem }} isShowActivity={false} style={styles.image} />
                    </TouchableOpacity>
                </View>

                <View style={styles.info}>
                    <View style={styles.details}>
                        <Text style={styles.infoTitle}>{this.props.product.nome}</Text>

                        <Text style={styles.infoDescription} numberOfLines={5}>
                            {this.props.product.descricao}
                        </Text>

                        <Text style={styles.price}>Valor unitário: {numeral(this.props.product.valor_final).format('$ 0,0.00')}</Text>
                    </View>

                    <View style={styles.actions}>
                        {/* <View>
                            {
                                this.hasOrders() != 0 &&
                                <Bell number={this.hasOrders()} style="secondary" />
                            }
                        </View> */}

                        <Btn text='ADICIONAR' onPress={() => this.addProduct()} />
                    </View>
                </View>

                {this.props.product.variantes.length === 0 && (
                    <SmallModal visible={this.state.modalVisible} closeModal={() => this.closeModal()}>
                        <NoVariants product={this.props.product} closeModal={() => this.closeModal()} />
                    </SmallModal>
                )}

                {this.props.product.variantes.length !== 0 && (
                    <LargeModal visible={this.state.modalVisible} closeModal={() => this.closeModal()}>
                        <Variants
                            variants={this.props.product.variantes}
                            product={this.props.product}
                            closeModal={() => this.closeModal()}
                        />
                    </LargeModal>
                )}
            </View>
        );
    }
}

export default connect(
    state => ({
        comandas: state.comandasReducer.comandas
    }),
    {}
)(ProductItem);

const styles = {
    container: {
        marginBottom: 16,
        flexDirection: 'row',
        height: 192,
        borderRadius: 4,
        elevation: 2
    },
    imageContainer: {
        width: 256,
        height: '100%',
        backgroundColor: colors.secondaryColor,
        position: 'relative'
    },
    image: {
        height: '100%',
        width: '100%',
        resizeMode: 'contain'
    },
    price: {
        fontSize: 16,
        fontFamily: fonts.default,
        color: colors.defaultText,
        fontWeight: 'bold'
    },
    info: {
        backgroundColor: '#fff',
        padding: 24,
        flex: 1,
        flexDirection: 'row',
        alignItems: 'center'
    },
    details: {
        flex: 1
    },
    infoTitle: {
        color: colors.defaultText,
        fontSize: 18,
        fontFamily: fonts.secondary,
        marginBottom: 24
    },
    infoDescription: {
        color: colors.defaultText,
        fontSize: 14,
        fontFamily: fonts.default,
        marginBottom: 24
    },
    infoValue: {
        color: colors.defaultText,
        fontSize: 14,
        fontFamily: fonts.secondary,
        fontWeight: 'bold'
    },
    actions: {
        marginLeft: 16
    },
    modalContent: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        alignItems: 'center'
    },
    modalBody: {
        backgroundColor: '#fff',
        borderRadius: 5,
        flex: 1,
        alignSelf: 'stretch'
    },
    modalNoVariantBody: {
        backgroundColor: '#fff',
        borderRadius: 5
    },
    modalContentLarge: {
        padding: 40
    },
    modalContentSmall: {
        // paddingHorizontal: wp('20%'),
        // paddingVertical: hp('20%')
    }
};
