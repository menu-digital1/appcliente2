import React, { Component } from "react";
import { Text, View, FlatList } from "react-native";

import BookCategoriesItem from "./book_categories_item";
import { colors } from "../styles/colors";
import { fonts } from "../styles/app";

export default class BookCategories extends Component {
    render() {
        return (
            <View>
                <Text style={styles.letter}>{this.props.category[0]}</Text>

                <FlatList
                    extraData={this.props}
                    data={this.props.category[1]}
                    keyExtractor={(i, key) => key.toString()}
                    renderItem={b => (
                        <BookCategoriesItem
                            currentCategory={this.props.currentCategory}
                            category={b.item}
                            findBooks={this.props.findBooks}
                        />
                    )}
                />
            </View>
        );
    }
}

const styles = {
    letter: {
        fontFamily: fonts.secondary,
        color: colors.textInPrimary,
        fontSize: 24
    }
};
