import React, { Component } from 'react';
import { Text, View, FlatList, TouchableOpacity } from 'react-native';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/FontAwesome';

import { colors } from '../styles/colors';
import { fonts } from '../styles/app';
import VariantItem from './variant_item';

export default class VariantHeader extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selected: [],
            open: false
        };

        this.setSelected = this.setSelected.bind(this);
    }

    // componentWillReceiveProps(props) {
    //     var selected = [];

    //     if (props.productQuantity <= 0) {
    //         selected = [];
    //     } else {
    //         if (this.props.currentComanda.products) {
    // 			var product = _.find(
    // 				this.props.currentComanda.products,
    // 				p => p.id == this.props.product._id
    // 			);

    // 			if (product) {
    // 				if (product.variants) {
    // 					var selected = _.filter(product.variants, v => v.selected);
    //                 }
    // 			}
    // 		}
    //     }

    //     this.setState({ selected });
    // }

    setSelected(selected) {
        this.setState({ selected });
    }

    toggleHeader() {
        this.setState({ open: !this.state.open });
    }

    render() {
        return (
            <View>
                <TouchableOpacity onPress={() => this.toggleHeader()} style={styles.container}>
                    <View style={styles.toggleContainer}>
                        <Icon name={this.state.open ? 'chevron-up' : 'chevron-down'} style={styles.toggleButton} />

                        <Text style={styles.title}>{this.props.variant.nome}</Text>
                    </View>

                    {this.props.variant.tipo == 'true' && (
                        <Text style={styles.subtitle}>Máximo {this.props.variant.maxSelect} por item</Text>
                    )}

                    {this.props.variant.tipo == 'false' && <Text style={styles.subtitle}>Escolha somente 1 opção</Text>}
                </TouchableOpacity>

                {this.state.open && (
                    <FlatList
                        extraData={[this.state, this.props]}
                        data={this.props.variant.itens_variante}
                        keyExtractor={(i, key) => key.toString()}
                        renderItem={i => (
                            <VariantItem
                                item={i.item}
                                variant={this.props.variant}
                                currentComanda={this.props.currentComanda}
                                product={this.props.product}
                                comandas={this.props.comandas}
                                setComandas={this.props.setComandas}
                                // productQuantity={this.props.productQuantity}
                                // selected={this.state.selected}
                                // setSelected={this.setSelected}
                            />
                        )}
                    />
                )}
            </View>
        );
    }
}

const styles = {
    container: {
        backgroundColor: '#F7F7F7',
        padding: 16,
        marginTop: 16,
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center'
    },
    title: {
        color: colors.defaultText,
        fontSize: 16,
        fontFamily: fonts.default,
        fontWeight: 'bold'
    },
    subtitle: {
        color: colors.defaultText,
        fontSize: 14,
        fontFamily: fonts.default
    },
    toggleContainer: {
        flexDirection: 'row',
        alignItems: 'center'
    },
    toggleButton: {
        color: colors.defaultText,
        marginRight: 16,
        fontSize: 24
    }
};
