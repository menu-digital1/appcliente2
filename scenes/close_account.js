import React, { Component } from 'react';
import { Text, View, FlatList, TextInput, Alert, Modal, ScrollView, Image } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import { Actions } from 'react-native-router-flux';
import numeral from 'numeral';

import Comanda from '../models/Comanda';
import Checkout from '../models/Checkout';
import Table from '../models/Table';
import Btn from '../components/btn';
import CloseAccountItem from './close_account_item';
import InternetMonitor from './internet_monitor';
import WaiterCall from '../components/waiter_call';
import DonutLoading from '../components/donut_loading';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';
import Order from '../models/Order';
import Route from '../models/Route';
import SmallModal from '../components/small_modal';

export default class CloseAccount extends Component {
    constructor(props) {
        super(props);

        this.state = {
            comandas: [],
            comandasCobrar: [],
            total: 0,
            table: null,
            modalVisible: false,
            loadingComandas: true,
            orders: [],
            checkedComandas: [],
            finalizing: false
        };

        this.calcTotal = this.calcTotal.bind(this);
        this.toggleSelectedComanda = this.toggleSelectedComanda.bind(this);
        this.togglePayComission = this.togglePayComission.bind(this);
    }

    componentDidMount() {
        // this.findComandas();

        this.setState({ loadingComandas: true });

        Table.getTable().then(table => {
            this.setState({ table: table });

            comandas = Comanda.getComandas();

            var checkedComandas = [];
            for (var c of comandas) {
                checkedComandas.push({
                    number: Number(c.number),
                    payComission: true
                });
            }

            this.setState({ checkedComandas });

            Order.getOrders(comandas).then(orders => {
                orders = _.orderBy(orders, o => o[0]);

                this.setState(
                    {
                        orders,
                        loadingComandas: false
                    },
                    () => {
                        this.calcTotal();
                    }
                );
            });
        });
    }

    toggleSelectedComanda(comanda, payComission) {
        var checkedComandas = this.state.checkedComandas;

        if (_.find(checkedComandas, c => c.number == comanda)) {
            checkedComandas = _.reject(checkedComandas, c => c.number == comanda);
        } else {
            checkedComandas.push({
                number: comanda,
                payComission
            });
        }

        this.setState({ checkedComandas }, () => {
            this.calcTotal();
        });
    }

    uncheckComanda(e) {
        this.setState(
            {
                comandasCobrar: this.state.comandasCobrar.filter(function(c) {
                    console.log(c + '/' + e);
                    return c !== e;
                })
            },
            () => {
                console.log(this.state.comandasCobrar);
            }
        );
    }

    // calcTotal(total, checked, comandaNumber){
    //     if(checked == true){
    //         var totalgeral = this.state.totalgeral + (total * 1.1);

    //         this.setState({comandasCobrar: this.state.comandasCobrar.concat([comandaNumber])});
    //     } else {
    //         var totalgeral = this.state.totalgeral - (total * 1.1);

    //         this.uncheckComanda(comandaNumber);
    //     }

    //     this.setState({ totalgeral: totalgeral });
    // }

    calcTotal() {
        var total = 0;

        for (var o of this.state.orders) {
            var checkedComanda = _.find(this.state.checkedComandas, c => c.number == o[0]);

            if (checkedComanda) {
                for (var p of o[1]) {
                    if (p.status !== 'canceled') {
                        if (checkedComanda.payComission) {
                            total += p.value * 1.1;
                        } else {
                            total += p.value;
                        }
                    }
                }
            }
        }

        this.setState({ total });
    }

    pedirConta() {
        Alert.alert(
            'CONFIRMAR',
            'Tem certeza que deseja pedir a conta para essas comandas?',
            [
                { text: 'Não' },
                {
                    text: 'Sim',
                    onPress: () => {
                        this.setState({ finalizing: true });

                        Checkout.pedirConta(this.state.checkedComandas, this.state.table).then(() => {
                            var comandas = Comanda.getComandas();

                            for (var c of this.state.checkedComandas) {
                                comandas = _.reject(comandas, co => co.number == c.number);
                            }

                            Comanda.setComandas(comandas);

                            this.setState({
                                finalizing: false,
                                modalVisible: true
                            });
                        });
                    }
                }
            ],
            { cancelable: false }
        );
    }

    closeModal() {
        Actions.reset('home');
        this.setState({ modalVisible: false });
    }

    findComandas() {
        this.setState({ comandas: Comanda.getComandas() });
    }

    togglePayComission(comanda, payComission) {
        var checkedComandas = this.state.checkedComandas;

        var index = _.findIndex(checkedComandas, c => c.number == comanda);

        checkedComandas[index].payComission = payComission;

        this.setState({ checkedComandas }, () => {
            this.calcTotal();
        });
    }

    back() {
        Actions.reset('products');
    }

    render() {
        return (
            <View style={styles.container}>
                <InternetMonitor />

                <View style={styles.header}>
                    <Text style={styles.h1}>Minha Conta</Text>
                </View>

                {this.state.loadingComandas && <DonutLoading />}

                {this.state.orders.length === 0 && !this.state.loadingComandas && (
                    <View style={styles.emptyDataContainer}>
                        <Image source={require('../img/bill.png')} style={styles.emptyDataImage} />

                        <Text style={styles.emptyDataText}>Nenhum pedido encontrado</Text>
                    </View>
                )}

                {this.state.orders.length !== 0 && !this.state.loadingComandas && (
                    <FlatList
                        style={styles.comandasList}
                        data={this.state.orders}
                        keyExtractor={(i, key) => key.toString()}
                        renderItem={c => (
                            <CloseAccountItem
                                table={this.state.table}
                                toggleSelectedComanda={this.toggleSelectedComanda}
                                togglePayComission={this.togglePayComission}
                                comanda={c.item}
                            />
                        )}
                    />
                )}

                <View style={styles.footer}>
                    <Text style={styles.total}>Total: {numeral(this.state.total).format('$ 0,0.00')}</Text>

                    <View style={styles.actions}>
                        <Btn text='VOLTAR AO CARDÁPIO' type='secondary' onPress={() => this.back()} />

                        <Btn
                            text='PEDIR A CONTA'
                            disabled={this.state.loadingComandas || this.state.total == 0}
                            loading={this.state.finalizing}
                            onPress={() => this.pedirConta()}
                        />
                    </View>
                </View>

                <Modal animationType='fade' transparent visible={this.state.modalVisible}>
                    <View style={styles.modalContent}>
                        <View style={styles.modalBody}>
                            <Image source={require('../img/positive-vote.png')} style={styles.modalIcon} />

                            <Text style={styles.modalH1}>SUCESSO!</Text>

                            <Text style={styles.modalH2}>O garçom vai trazer a sua conta.</Text>

                            <Btn text='OK' onPress={() => this.closeModal()} />
                        </View>
                    </View>
                </Modal>
            </View>
        );
    }
}

const styles = {
    container: {
        // paddingVertical: hp("5%"),
        // paddingHorizontal: wp("5%"),
        padding: 40,
        flex: 1
    },
    header: {
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    h1: {
        fontFamily: fonts.secondary,
        fontSize: 24,
        color: colors.defaultText
    },
    comandasList: {
        flex: 1
    },
    total: {
        textAlign: 'right',
        fontFamily: fonts.default,
        color: colors.defaultText,
        fontSize: 18,
        marginTop: 25,
        fontWeight: 'bold'
    },
    actions: {
        marginTop: 25,
        flexDirection: 'row',
        justifyContent: 'space-between'
    },
    emptyDataContainer: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    emptyDataImage: {
        height: 64,
        marginBottom: 15,
        resizeMode: 'contain'
    },
    emptyDataText: {
        textAlign: 'center',
        fontSize: 24,
        fontFamily: fonts.secondary,
        color: '#bababa'
    },
    modalContent: {
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.8)',
        alignItems: 'center',
        justifyContent: 'center'
    },
    modalBody: {
        backgroundColor: '#fff',
        borderRadius: 5,
        width: 770,
        padding: 40
    },
    modalIcon: {
        height: 90,
        resizeMode: 'contain',
        marginBottom: 24,
        alignSelf: 'center'
    },
    modalH1: {
        fontFamily: fonts.secondary,
        color: colors.defaultText,
        fontSize: 26,
        textAlign: 'center'
    },
    modalH2: {
        fontFamily: fonts.default,
        color: colors.defaultText,
        fontSize: 20,
        textAlign: 'center',
        marginBottom: 16
    }
};
