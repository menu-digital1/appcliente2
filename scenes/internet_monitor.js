import React, { Component } from 'react';
import { Text, Modal, Image, View } from 'react-native';

import Network from '../models/Network';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';

export default class InternetMonitor extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isConnected: true
		};
	}

	componentDidMount() {
		Network.monitor(isConnected => {
			this.setState({ isConnected });
		});
	}

	render() {
		return (
			<Modal animationType='fade' transparent visible={!this.state.isConnected}>
				<View style={styles.modal}>
					<View style={styles.modalContent}>
						<Image source={require('../img/no-wifi.png')} style={styles.image} />

						<Text style={styles.text}>Não há conexão com a internet no momento.</Text>

						<Text style={styles.text}>Se precisar fazer algum pedido, por favor, chame o garçom.</Text>
					</View>
				</View>
			</Modal>
		);
	}
}

const styles = {
	modal: {
		flex: 1,
		backgroundColor: 'rgba(0, 0, 0, 0.8)',
		alignItems: 'center',
		justifyContent: 'center'
	},
	modalContent: {
		backgroundColor: '#fff',
		borderRadius: 5,
        width: 770,
        padding: 40,
	},
	image: {
		height: 128,
		resizeMode: 'contain'
	},
	text: {
		fontFamily: fonts.secondary,
		color: colors.defaultText,
		fontSize: 24,
		marginVertical: 16,
		textAlign: 'center'
	}
};
