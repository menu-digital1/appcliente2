import React, { Component } from "react";
import { FlatList, View, TextInput, Image, Text, TouchableOpacity } from "react-native";
import Icon from "react-native-vector-icons/FontAwesome";

import Book from "../models/Book";
import BookCategories from "./book_categories";
import BookItem from "./book_item";
import Btn from "../components/btn";
import { colors } from "../styles/colors";
import { fonts, app } from "../styles/app";
import DonutLoading from "../components/donut_loading";
import OrdersBar from "../components/order_bar";
import { Actions } from "react-native-router-flux";
import InternetMonitor from "./internet_monitor";
import TableBar from "../components/table_bar";

export default class Books extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loadingCategories: true,
            loadingBooks: true,
            currentCategory: null,
            books: [],
            categories: [],
            search: ""
        };

        this.findBooks = this.findBooks.bind(this);
    }

    componentDidMount() {
        this.setState({ loadingCategories: true });

        this.findCategories().then(() => {
            if (this.state.categories.length !== 0) {
                this.findBooks(this.state.categories[0][1][0][0]);
            } else {
                this.setState({
                    loadingBooks: false,
                    loadingCategories: false
                });
            }

            this.setState({ loadingCategories: false });
        });
    }

    findCategories() {
        return new Promise((resolve, reject) => {
            Book.findCategories().then(categories => {
                this.setState({ categories });

                resolve(true);
            });
        });
    }

    findBooks(category) {
        this.setState({
            loadingBooks: true,
            currentCategory: category
        });

        Book.find({ category }).then(books => {
            this.setState({
                books,
                loadingBooks: false
            });
        });
    }

    searchBooks(search) {
        if (search) {
            this.setState({ loadingBooks: true });

            Book.find({ search }).then(books => {
                this.setState({
                    books,
                    loadingBooks: false
                });
            });
        } else {
            this.findBooks(this.state.categories[0][1][0][0]);
        }
    }

    back() {
        Actions.reset("home");
    }

    render() {
        return (
            <View style={app.mainContainer}>
                <InternetMonitor />

                <View style={app.menuContainer}>
                    <TableBar onBack={() => this.back()} />

                    <View style={app.menuContainerInner}>
                        <FlatList
                            extraData={this.state}
                            data={this.state.categories}
                            keyExtractor={(i, key) => key.toString()}
                            contentContainerStyle={{ justifyContent: "center", flex: 1 }}
                            renderItem={b => (
                                <BookCategories
                                    currentCategory={this.state.currentCategory}
                                    category={b.item}
                                    findBooks={this.findBooks}
                                />
                            )}
                        />
                    </View>
                </View>

                <View style={app.mainContent}>
                    <View style={app.mainHeader}>
                        <Text style={app.h2}>LIVROS</Text>

                        <OrdersBar mini />
                    </View>

                    <View style={styles.searchContainer}>
                        <TextInput
                            value={this.state.search}
                            style={styles.searchInput}
                            placeholderTextColor="#9a9a9a"
                            placeholder="Buscar por título ou autor"
                            onChangeText={search => this.setState({ search })}
                        />

                        <TouchableOpacity onPress={() => this.searchBooks(this.state.search)}>
                            <View style={styles.searchButton}>
                                <Icon name="search" style={styles.searchButtonIcon} />

                                <Text style={styles.searchButtonText}>Buscar</Text>
                            </View>
                        </TouchableOpacity>
                    </View>

                    {!this.state.loadingBooks && this.state.books.length !== 0 && (
                        <FlatList
                            numColumns={4}
                            style={styles.booksList}
                            data={this.state.books}
                            keyExtractor={(i, key) => key.toString()}
                            renderItem={b => <BookItem book={b.item} />}
                        />
                    )}

                    {!this.state.loadingBooks && this.state.books.length === 0 && (
                        <View style={styles.emptyDataContainer}>
                            <Image
                                source={require("../img/open-magazine.png")}
                                style={styles.emptyDataImage}
                            />

                            <Text style={styles.emptyDataText}>Nenhum livro encontrado</Text>
                        </View>
                    )}

                    {this.state.loadingBooks && (
                        <View style={{ marginVertical: '15%' }}>
                            <DonutLoading />
                        </View>
                    )}
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        flexDirection: "row"
    },
    menuContainer: {
        height: "100%",
        backgroundColor: colors.primaryColor,
        paddingHorizontal: 40,
        paddingBottom: 40,
        paddingTop: 120,
        width: 400
    },
    content: {
        flex: 1,
        position: "relative"
    },
    logo: {
        height: 250,
        resizeMode: "contain",
        alignSelf: "center"
    },
    contentInner: {
        flex: 1,
        paddingHorizontal: 40,
        paddingTop: 120,
        paddingBottom: 40
    },
    wrapper: {
        flex: 1
    },
    header: {
        marginBottom: 15
    },
    h1: {
        fontFamily: fonts.secondary,
        color: colors.defaultText,
        fontSize: 30,
        marginBottom: 5
    },
    h3: {
        fontFamily: fonts.default,
        color: colors.defaultText,
        fontSize: 16,
        marginVertical: 16
    },
    searchContainer: {
        flexDirection: "row",
        marginVertical: 16
    },
    searchInput: {
        backgroundColor: "#fff",
        padding: 12,
        borderRadius: 2,
        fontFamily: fonts.default,
        color: colors.defaultText,
        fontSize: 18,
        flex: 1,
        height: 56
    },
    searchButton: {
        backgroundColor: colors.tertiaryColor,
        borderRadius: 2,
        padding: 16,
        alignItems: "center",
        justifyContent: "center",
		height: 56,
		flexDirection: 'row',
    },
    searchButtonIcon: {
        fontSize: 18,
		color: colors.textInTertiary,
		marginRight: 8
    },
    searchButtonText: {
        fontSize: 18,
        color: colors.textInTertiary,
        fontFamily: fonts.default,
    },
    booksList: {
        marginHorizontal: -32,
        marginVertical: 25
    },
    emptyDataContainer: {
        marginVertical: 48,
        alignItems: "center",
        justifyContent: "center"
    },
    emptyDataImage: {
        height: 64,
        marginBottom: 15,
        resizeMode: "contain"
    },
    emptyDataText: {
        textAlign: "center",
        fontSize: 24,
        fontFamily: fonts.secondary,
        color: "#bababa"
    },
    backButton: {
        position: "absolute",
        top: 20,
        left: 20
    },
    backIcon: {
        color: colors.textInPrimary,
        fontSize: 32
    }
};
