import React, { Component } from 'react';
import { View, Alert, Text, TouchableOpacity, Image, ActivityIndicator } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { RNCamera } from 'react-native-camera';
import { connect } from 'react-redux';
import _ from 'lodash';
import Icon from 'react-native-vector-icons/FontAwesome';

import NumericPad from '../components/numeric_pad';
import Comanda from '../models/Comanda';
import InternetMonitor from './internet_monitor';
import Table from '../models/Table';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';
import { updateComandas } from '../reducers/actions';
import Api from '../services/api';

class EnterComanda extends Component {
    constructor(props) {
        super(props);

        this.state = {
            comandaNumber: '',
            loading: false,
            screenReady: false,
            cameraReady: false
        };
    }

    componentDidMount() {
        setTimeout(() => {
            this.setState({ screenReady: true });
        }, 500);
    }

    submitComanda() {
        var error = false;

        if (!this.state.comandaNumber) {
            Alert.alert('ERRO', 'Por favor, entre com o número da Comanda', [{ text: 'OK' }], {
                cancelable: false
            });

            error = true;
        }

        if (!error) {
            this.setState({ loading: true });

            this.camera.pausePreview();

            Table.getTable().then(table => {
                Comanda.checkComanda(this.state.comandaNumber, table).then(
                    response => {
                        if (response.change_table == true) {
                            Alert.alert(
                                'AVISO',
                                'Esta comanda já está associada a outra mesa. Gostaria de trocar para esta mesa?',
                                [
                                    {
                                        text: 'Não',
                                        onPress: () => {
                                            this.camera.resumePreview();

                                            this.setState({
                                                loading: false
                                            });
                                        }
                                    },
                                    {
                                        text: 'Sim',
                                        onPress: () => {
                                            this.addComanda(this.state.comandaNumber, table).then(() => {
                                                Actions.reset('comandas');
                                            });
                                        }
                                    },                                    
                                ],
                                { cancelable: false }
                            );
                        } else {
                            this.addComanda(this.state.comandaNumber, table).then(() => {
                                Actions.reset('comandas');
                            });
                        }
                    },
                    error => {
                        Alert.alert(
                            'ERRO',
                            error,
                            [
                                {
                                    text: 'OK',
                                    onPress: () => {
                                        this.camera.resumePreview();
                                    }
                                }
                            ],
                            { cancelable: false }
                        );

                        this.setState({
                            loading: false
                        });
                    }
                );
            });
        }
    }

    addComanda(commandaNumber, tableNumber) {
        return new Promise(resolve => {
            commandaNumber = Number(commandaNumber);

            var comandas = this.props.comandas.slice();

            var data = {
                code: commandaNumber,
                table: tableNumber
            };

            Api.post('link-comanda', data).then(() => {
                if (!_.find(comandas, c => c.number == commandaNumber)) {
                    comandas.push({ number: commandaNumber });

                    this.props.updateComandas(comandas);

                    resolve(true);
                } else {
                    resolve(true);
                }
            });
        });
    }

    onQRCodeDetected(code) {
        this.addComanda(code);
    }

    setComandaNumber(value) {
        this.setState({ comandaNumber: value });
    }

    back() {
        Actions.reset('home');
    }

    render() {
        return (
            <View style={styles.container}>
                <InternetMonitor />

                <TouchableOpacity
                    hitSlop={{
                        top: 40,
                        left: 40,
                        bottom: 40,
                        right: 40
                    }}
                    style={styles.backButton}
                    onPress={() => this.back()}
                >
                    <Icon name='arrow-left' style={styles.backButtonIcon} />
                </TouchableOpacity>

                <View style={styles.innerContainer}>
                    <View style={styles.header}>
                        <Image source={require('../img/qrcode.png')} style={styles.icon} />

                        <Text style={styles.title}>Aproxime o QR CODE da câmera ou digite o número que aparece na comanda:</Text>
                    </View>

                    <View style={styles.row}>
                        {!this.state.cameraReady && (
                            <View style={styles.loadingContainer}>
                                <ActivityIndicator size={64} color={colors.defaultText} />
                            </View>
                        )}

                        <View style={styles.cameraContainer}>
                            <View style={styles.cameraContent}>
                                {this.state.screenReady && (
                                    <RNCamera
                                        ref={ref => (this.camera = ref)}
                                        type={RNCamera.Constants.Type.front}
                                        captureAudio={false}
                                        style={styles.camera}
                                        androidCameraPermissionOptions={{
                                            title: 'Usar a câmera',
                                            message: 'Precisamos da permissão para usar a câmera',
                                            buttonPositive: 'Ok',
                                            buttonNegative: 'Cancelar'
                                        }}
                                        onGoogleVisionBarcodesDetected={({ barcodes }) => {
                                            this.onQRCodeDetected(barcodes[0].data);
                                        }}
                                        onCameraReady={() => this.setState({ cameraReady: true })}
                                    />
                                )}
                            </View>
                        </View>

                        <View style={{ flex: 1 }}>
                            <NumericPad
                                value={this.state.comandaNumber}
                                loading={this.state.loading}
                                onChange={comandaNumber => this.setState({ comandaNumber })}
                                onSubmit={() => this.submitComanda()}
                            />
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

export default connect(
    state => ({
        comandas: state.comandasReducer.comandas
    }),
    { updateComandas }
)(EnterComanda);

const styles = {
    container: {
        backgroundColor: colors.surface,
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative'
    },
    innerContainer: {
        padding: 24
    },
    header: {
        justifyContent: 'center',
        marginBottom: 32
    },
    row: {
        flexDirection: 'row',
        alignItems: 'center',
        position: 'relative'
    },
    icon: {
        height: 64,
        resizeMode: 'contain',
        marginBottom: 16,
        alignSelf: 'center',
        tintColor: colors.onSurface
    },
    title: {
        fontFamily: fonts.default,
        fontSize: 28,
        color: colors.onSurface,
        fontWeight: 'bold',
        textAlign: 'center'
    },
    cameraContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center'
    },
    cameraContent: {
        height: '40%',
        width: '85%',
        paddingBottom: 20
    },
    camera: {
        width: '100%',
        height: '100%'
    },
    keyboardContainer: {
        flex: 1,
        // paddingVertical: hp('2%'),
        // paddingHorizontal: wp('10%')
    },
    infoText: {
        fontFamily: fonts.secondary,
        fontSize: 16,
        // lineHeight: hp('4%'),
        textAlign: 'center',
        color: colors.defaultText,
        marginTop: 30,
        width: '80%'
    },
    backButton: {
        position: 'absolute',
        top: 16,
        left: 16
    },
    backButtonIcon: {
        fontSize: 48,
        color: colors.defaultText
    },
    loadingContainer: {
        position: 'absolute',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        zIndex: 9999,
        backgroundColor: '#fff',
        justifyContent: 'center',
        alignItems: 'center'
    }
};
