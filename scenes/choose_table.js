import React, { Component } from "react";
import { View, Text, Alert, ScrollView } from "react-native";
import { Actions } from "react-native-router-flux";

import { app, fonts } from "../styles/app";
import { colors } from "../styles/colors";
import Table from "../models/Table";
import Comanda from "../models/Comanda";
import NumericPad from "../components/numeric_pad";
import Route from "../models/Route";
import InternetMonitor from "./internet_monitor";

export default class ChooseTable extends Component {
    constructor(props) {
        super(props);

        this.state = {
            tableNumber: '',
            loading: false
        };
    }

    setTable() {
        var error = false;

        if (!this.state.tableNumber) {
            Alert.alert("ERRO", "Por favor, entre com o número da mesa", [{ text: "OK" }], {
                cancelable: false
            });

            error = true;
        }

        if (!error) {
            this.setState({ loading: true });

            Table.setTable(this.state.tableNumber).then(response => {
                if (response.error == true) {
                    Alert.alert("ERRO", response.message, [{ text: "OK" }], { cancelable: false });
                    error = true;
                    this.setState({ loading: false });
                } else {
                    Actions.home({ type: 'reset' });
                }
            });
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <InternetMonitor />

                <View style={styles.innerContainer}>
                    <Text style={styles.infoText}>Digite o número da mesa para inserí-la no sistema:</Text>

                    <NumericPad
                        value={this.state.tableNumber}
                        loading={this.state.loading}
                        onChange={tableNumber => this.setState({ tableNumber })}
                        onSubmit={() => this.setTable()}
                    />
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: colors.surface
    },
    innerContainer: {
        width: "40%"
    },
    infoText: {
        fontFamily: fonts.secondary,
        fontSize: 34,
        lineHeight: 46,
        textAlign: "center",
        color: colors.defaultText,
        marginBottom: 16
    },
    keyboardContainer: {
        flex: 1,
        // paddingVertical: hp("2%"),
        // paddingHorizontal: wp("10%")
    },
    tableInput: {
        borderWidth: 1,
        borderColor: colors.lightGray,
        textAlign: "center",
        fontFamily: fonts.default,
        fontSize: 40,
        padding: 15,
        marginBottom: 5
    },
    keyboardRow: {
        marginHorizontal: -5
    },
    keyboardButton: {
        backgroundColor: colors.gray,
        fontFamily: fonts.default,
        fontWeight: "bold",
        color: colors.defaultText,
        padding: 5,
        marginHorizontal: 5
    }
};
