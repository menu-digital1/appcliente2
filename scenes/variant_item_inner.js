import React, { Component } from 'react'
import { Text, View } from 'react-native'
import NumericInput from 'react-native-numeric-input'
import { connect } from 'react-redux';
import _ from 'lodash';

import { updateComandas } from '../reducers/actions';
import Order from '../models/Order';
import Comanda from '../models/Comanda';

class VariantItemInner extends Component {

    constructor(props) {
        super(props);
    
        this.state = {
            currentVariant: {},
            variantQuantity: 0,
        };
    }

    componentDidMount() {
        this.getVartiantQuantity();
    }

    getVartiantQuantity() {
        var currentProduct = _.find(this.props.currentComanda.products, p => p.id == this.props.product['_id']['$oid']);

        if (currentProduct && currentProduct.variants) {
            this.setState({ currentVariant: _.find(currentProduct.variants, v => v.id == this.props.variant['_id']['$oid']) });
        }
    }

    setVariantQuantity(value) {
        var currentVariant = this.state.currentVariant;

        if (!currentVariant) {
            currentVariant = {};
        }

        currentVariant.id = this.props.variant['_id']['$oid'];
        currentVariant.name = this.props.variant.nome;
        currentVariant.quantity = value;

        this.setState({ currentVariant });

        var currentComanda = this.props.currentComanda;

        var productIndex = _.findIndex(currentComanda.products, p => p.id == this.props.product['_id']['$oid']);
        var variantIndex = _.findIndex(currentComanda.products[productIndex].variants, v => v.id == this.props.variant['_id']['$oid']);

        if (variantIndex == -1) {
            currentComanda.products[productIndex].variants.push(currentVariant);
        } else {
            if (value <= 0) {
                currentComanda.products[productIndex].variants.splice(variantIndex, 1);
            } else {
                currentComanda.products[productIndex].variants[variantIndex] = currentVariant;
            }
        }

        Comanda.updateComanda(currentComanda);

        console.log(this.props.comandas);

        // Order.setVariantQuantity(this.props.comandaIndex, this.props.productId, this.props.variant, value);
    }

    render() {
        return (
            <View>
                <Text>{this.props.variant.nome}</Text>
                <Text>+{this.props.variant.valor_adicional}</Text>

                <NumericInput 
                    minValue={0}
                    maxValue={this.props.maxSelect}
                    value={this.state.currentVariant.quantity}
                    initValue={this.state.currentVariant.quantity}
                    onChange={value => this.setVariantQuantity(value)}
                />
            </View>
        )
    }
}

export default connect(state => ({
    comandas: state.comandasReducer.comandas,
}), {
    updateComandas,
})(VariantItemInner);