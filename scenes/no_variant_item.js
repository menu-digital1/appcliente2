import React, { Component } from 'react';
import { Text, View } from 'react-native';
import numeral from 'numeral';
import _ from 'lodash';
import { connect } from 'react-redux';

import NumberSpinner from '../components/number_spinner';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';
import { updateComandas } from '../reducers/actions';

class NoVariantItem extends Component {
	constructor(props) {
		super(props);

		this.state = {
			quantity: 0,
		};
	}

	componentDidMount() {
		// var quantity = this.state.quantity;

		// if (this.props.comanda.products) {
		// 	var product = _.find(
		// 		this.props.comanda.products,
		// 		p => p.id == this.props.product._id
		// 	);

		// 	if (product) {
		// 		quantity = product.quantity;
		// 	}
		// }

		// this.setState({ quantity });
	}

	setProductQuantity(quantity) {
		var data = {
			id: this.props.product._id,
			name: this.props.product.nome,
			value: this.props.product.valor_final,
			tipo: this.props.product.tipo,
			quantity: quantity
		};

		var comandas = this.props.comandas.slice();

		if (!comandas[this.props.index].products) {
			comandas[this.props.index].products = [];
		}

		var productIndex = _.findIndex(comandas[this.props.index].products, p => p.id == this.props.product._id);

		if (productIndex == -1) {
			comandas[this.props.index].products.push(data);
		} else {
			if (quantity <= 0) {
				comandas[this.props.index].products.splice(productIndex, 1);
			} else {
				comandas[this.props.index].products[productIndex] = {
					id: data.id,
					name: data.name,
					value: data.value,
					tipo: data.tipo,
					quantity: data.quantity
				};
			}
		}

		this.props.updateComandas(comandas);
	}

	getQuantity() {
		var productQuantity = 0;

		if (this.props.comandas[this.props.index].products) {
			var product = _.find(this.props.comandas[this.props.index].products, p => p.id == this.props.product._id);

			if (product) {
				productQuantity = product.quantity;
			}
		}

		return productQuantity;
	}

	getValue() {
		var productValue = 0;

		if (this.props.comandas[this.props.index].products) {
			var product = _.find(this.props.comandas[this.props.index].products, p => p.id == this.props.product._id);

			if (product) {
				productValue = product.value * product.quantity;
			}
		}

		return productValue;
	}

	render() {
		return (
			<View style={styles.comandaItem}>
				<Text style={styles.comandaNumber}>
					COMANDA {numeral(this.props.comanda.number).format('00')}
				</Text>

				<View style={styles.commandaSpinner}>
					<NumberSpinner
						value={this.getQuantity()}
						onChange={value => this.setProductQuantity(value)}
						min={0}
					/>
				</View>

				<Text style={styles.comandaValue}>
					{numeral(this.getValue()).format('$ 0,0.00')}
				</Text>
			</View>
		);
	}
}

export default connect(
	state => ({
		comandas: state.comandasReducer.comandas
	}),
	{ updateComandas }
)(NoVariantItem);

const styles = {
	comandaItem: {
		paddingVertical: 15,
		paddingHorizontal: 40,
		borderWidth: 1,
		borderColor: '#ebe4e0',
		flexDirection: 'row',
		alignItems: 'center'
	},
	comandaNumber: {
		color: colors.secondaryColor,
		fontSize: 20,
		fontFamily: fonts.secondary,
		flex: 1
	},
	commandaSpinner: {
		flex: 1,
        textAlign: 'center',
	},
	comandaValue: {
		textAlign: 'right',
		fontSize: 22,
		fontFamily: fonts.default,
		color: colors.defaultText,
		fontWeight: 'bold',
		flex: 1
	}
};
