import React, { Component } from 'react';
import { FlatList, View, Alert, Image, Text } from 'react-native';
import { Actions } from 'react-native-router-flux';
import { connect } from "react-redux";

import Btn from '../components/btn';
import ComandaItem from './comanda_item';
import InternetMonitor from './internet_monitor';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';
import { updateComandas } from '../reducers/actions';

class Comandas extends Component {
	constructor(props) {
		super(props);

		this.state = {
			comandas: []
		};
	}

	componentDidMount() {
		// Comanda.clearComandas().then(() => {
		//     this.findComandas();
		// });

		// this.findComandas();
	}

	// findComandas() {
	// 	this.setState({ comandas: Comanda.getComandas() });
	// }

	addNewComanda() {
		Actions.enterComanda();
	}

	onComandaRemoved() {
		this.findComandas();
	}

	confirmComandas() {
		Alert.alert(
			'CONFIRMAR',
			'Tem certeza que deseja continuar com essas Comandas?',
			[
				{ text: 'Não' },
				{
					text: 'Sim',
					onPress: () => {
						Actions.home({ type: 'reset' });
					}
				}
			],
			{ cancelable: false }
		);
	}

	render() {
		return (
			<View style={styles.container}>
				<InternetMonitor />

				<View style={styles.card}>
					<View style={styles.titleImgContainer}>
						<Image source={require('../img/positive-vote.png')} style={styles.titleImg} />

						<Text style={styles.h2}>COMANDAS CADASTRADAS</Text>
					</View>

					{this.props.comandas.length != 0 && (
						<FlatList
							horizontal
							data={this.props.comandas}
							contentContainerStyle={styles.comandasList}
							keyExtractor={(i, key) => key.toString()}
							renderItem={c => <ComandaItem comanda={c.item} />}
						/>
					)}

					{this.props.comandas.length == 0 && (
						<View style={styles.emptyDataContainer}>
							<Image source={require('../img/qrcode.png')} style={styles.emptyDataIcon} />

							<Text style={styles.emptyDataText}>Nenhuma comanda cadastrada</Text>
						</View>
					)}

					<View style={styles.actions}>
						<View style={styles.actionButton}>
							<Btn text='NOVA COMANDA' type='secondary' onPress={() => this.addNewComanda()} />
						</View>

						<View style={styles.actionButton}>
							<Btn text='OK' disabled={this.props.comandas.length === 0} onPress={() => this.confirmComandas()} />
						</View>
					</View>
				</View>
			</View>
		);
	}
}

export default connect(
	state => ({
		comandas: state.comandasReducer.comandas
	}),
	{ updateComandas }
)(Comandas);

const styles = {
	container: {
		flex: 1,
		backgroundColor: colors.primary
	},
	card: {
		backgroundColor: colors.surface,
		marginHorizontal: 128,
		marginVertical: 64,
		flex: 1,
		padding: 40,
		shadowColor: '#000',
		shadowOffset: {
			width: 0,
			height: 5
		},
		shadowOpacity: 0.34,
		shadowRadius: 6.27,
		elevation: 2,
		justifyContent: 'space-between',
		alignItems: 'center',
		borderRadius: 4
	},
	titleImgContainer: {},
	titleImg: {
		resizeMode: 'contain',
		height: 90,
		alignSelf: 'center'
	},
	h2: {
		fontFamily: fonts.default,
		fontSize: 26,
		textAlign: 'center',
		color: colors.onSurface,
		marginVertical: 16,
		fontWeight: 'bold'
	},
	actions: {
		flexDirection: 'row'
	},
	actionButton: {
		marginHorizontal: 32
	},
	emptyDataContainer: {
		flex: 1,
		justifyContent: 'center',
		alignItems: 'center'
	},
	emptyDataIcon: {
		height: 120,
		resizeMode: 'contain',
		marginBottom: 20,
		tintColor: '#bababa'
	},
	emptyDataText: {
		fontFamily: fonts.default,
		fontSize: 28,
		textAlign: 'center',
		color: '#bababa'
	},
	comandasList: {
		alignItems: 'center'
	}
};
