import React, { Component } from "react";
import { Text, View, Modal, TouchableOpacity, TouchableWithoutFeedback } from "react-native";
import ImageLoad from "react-native-image-placeholder";
import Icon from "react-native-vector-icons/FontAwesome";

import BookDetail from "./book_detail";
import { colors } from "../styles/colors";
import { fonts } from "../styles/app";
import SmallModal from "../components/small_modal";

export default class BookItem extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false
        };
    }

    closeModal() {
        this.setState({ modalVisible: false });
    }

    showDetails() {
        this.setState({ modalVisible: true });
    }

    render() {
        return (
            <View style={styles.container}>
                <TouchableOpacity onPress={() => this.showDetails()}>
                    <ImageLoad
                        source={{ uri: this.props.book.full_capa }}
                        isShowActivity={false}
                        style={styles.image}
                    />

                    <Text style={styles.title}>{this.props.book.titulo}</Text>

                    <Text style={styles.author}>{this.props.book.autor}</Text>
                </TouchableOpacity>

                <SmallModal visible={this.state.modalVisible} closeModal={() => this.closeModal()}>
                    <BookDetail book={this.props.book} />
                </SmallModal>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1 / 4,
        marginHorizontal: 32,
        marginBottom: 30
    },
    image: {
        height: 250,
        resizeMode: "cover",
        marginBottom: 5
    },
    title: {
        fontFamily: fonts.default,
        color: colors.defaultText,
        fontSize: 14,
        fontWeight: "bold"
    },
    title: {
        fontFamily: fonts.default,
        color: colors.defaultText,
        fontSize: 14
    },
    modalContent: {
        // paddingVertical: hp("20%"),
        // paddingHorizontal: wp("25%"),
        justifyContent: "center",
        flex: 1,
        backgroundColor: "rgba(0, 0, 0, 0.8)"
    },
    modalBody: {
        backgroundColor: "#fff",
        padding: 30,
        borderRadius: 5
    },
    closeButton: {
        position: "absolute",
        top: 10,
        right: 10
    },
    closeIcon: {
        color: "#BABABA",
        fontSize: 26
    }
};
