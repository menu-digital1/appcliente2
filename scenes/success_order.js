import React, { Component } from "react";
import { Text, View, Image } from "react-native";
import { Actions } from "react-native-router-flux";

import InternetMonitor from "./internet_monitor";
import { colors } from "../styles/colors";
import { fonts } from "../styles/app";
import Btn from "../components/btn";

export default class SuccessOrder extends Component {
    finalize() {
        Actions.reset("home");
    }

    render() {
        return (
            <View style={styles.container}>
                <InternetMonitor />

                <View style={styles.card}>
                    <Image source={require("../img/positive-vote.png")} style={styles.titleImg} />

                    <Text style={styles.h2}>SUCESSO!</Text>

                    <Text style={styles.h3}>Seu pedido foi enviado e já já será entregue.</Text>

                    <Btn text="OK" onPress={() => this.finalize()} />
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
        backgroundColor: colors.primary
    },
    card: {
        backgroundColor: colors.surface,
        marginHorizontal: 128,
        marginVertical: 64,
        flex: 1,
        padding: 40,
        shadowColor: "#000",
        shadowOffset: {
            width: 0,
            height: 5
        },
        shadowOpacity: 0.34,
        shadowRadius: 6.27,
        elevation: 2,
        justifyContent: "center",
        alignItems: "center",
        borderRadius: 4
    },
    titleImg: {
        resizeMode: "contain",
        height: 90,
        alignSelf: "center"
    },
    h2: {
        fontFamily: fonts.default,
        fontSize: 26,
        textAlign: "center",
        color: colors.onSurface,
        marginVertical: 16,
        fontWeight: "bold"
    },
    h3: {
        fontFamily: fonts.default,
        fontSize: 20,
        textAlign: "center",
        color: colors.onSurface,
        marginVertical: 24
    }
};
