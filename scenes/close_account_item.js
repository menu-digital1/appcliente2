import React, {Component} from 'react';
import { Text, View, FlatList, TouchableOpacity, Alert, CheckBox } from 'react-native';
import numeral from 'numeral';

import Comanda from '../models/Comanda';
import CloseAccountProduct from './close_account_product';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';
import Radio from '../components/radio';
import Table from '../models/Table';

export default class CloseAccountItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            orders: [],
            total: 0,
            check: true,
            table: null,
            payComission: true,
        };
    }

    totalRecalc = (id) => {
        this.setState({check: !this.state.check}, () => {
            this.props.propCalcTotal(this.state.total, this.state.check, this.props.comanda[0]);
        });        
    }    

    componentDidMount() {
        Table.getTable().then(table => this.setState({ table }));

        var total = 0
        for (var p of this.props.comanda[1]) {
            if (p.status !== 'canceled') {
                total += p.value;
            }
        }

        this.setState({ total });

        // this.props.propCalcTotal(total, this.state.check, this.props.comanda[0]);

        // Comanda.getOrders(this.props.comanda.number).then(orders => {
        //     this.setState({
        //         orders: orders,
        //         total: this.state.orders.total,
        //     });

        //     this.props.propCalcTotal(this.state.total, this.state.check, this.props.comanda.number);
        // });
    }

    toggleComanda() {
        this.setState({ check: !this.state.check });

        this.props.toggleSelectedComanda(this.props.comanda[0], this.state.payComission);
    }

    togglePayComission() {
        this.setState({ payComission: !this.state.payComission }, () => {
            this.props.togglePayComission(this.props.comanda[0], this.state.payComission);
        });
    }

    getTotal() {
        var total = 0;

        for (var p of this.props.comanda[1]) {
            if (p.status !== 'canceled') {
                total += p.value;
            }
        }

        if (this.state.payComission) {
            return total * 1.1;
        } else {
            return total;
        }
    }

    render() {
        return (
            <View style={styles.container}>
                <View style={styles.innerContainer}>
                    <View style={styles.comandaContainer}>
                        <View style={styles.check}>
                            <Radio
                                value={this.state.check}
                                onPress={() => this.toggleComanda()}
                                color={colors.textInSecondary}
                            />
                        </View>

                        <Text style={styles.comandaNumber}>COMANDA {this.props.comanda[0]}</Text>
                    </View>

                    <View style={styles.itemsContainer}>
                        <FlatList 
                            extraData={this.props}
                            data={this.props.comanda[1]}
                            keyExtractor={(item, index) => index.toString()}
                            listKey={(item, index) => `${this.props.comanda[0]}${index.toString()}`}
                            renderItem={p => 
                                <View style={styles.itemRow}>
                                    <Text style={[styles.item, styles.itemQuantity, p.item.status == 'canceled' ? styles.itemCanceled : '']}>
                                        {p.item.quantity}
                                    </Text>

                                    {
                                        p.item.table == this.props.table &&
                                        <Text style={[styles.item, styles.itemName, p.item.status == 'canceled' ? styles.itemCanceled : '']}>
                                            {p.item.name}
                                        </Text>
                                    }

                                    {
                                        p.item.table != this.props.table &&
                                        <Text style={[styles.item, styles.itemName, p.item.status == 'canceled' ? styles.itemCanceled : '']}>
                                            {p.item.name} (Mesa {numeral(p.item.table).format('00')})
                                        </Text>
                                    }
                                    
                                    <Text style={[styles.item, styles.itemPrice, p.item.status == 'canceled' ? styles.itemCanceled : '']}>
                                        {numeral(p.item.value).format('0,0.00')}
                                    </Text>
                                </View>
                            }
                        />

                        <View style={styles.itemFooter}>
                            {
                                this.state.payComission &&
                                <View style={styles.itemFooterInner}>
                                    <Text style={styles.itemFooterSubtitle}>Subtotal + Taxa de serviço</Text>

                                    <Text style={styles.itemFooterSubtotal}>{numeral(this.state.total).format('0,0.00')} + 10%</Text>
                                </View>
                            }

                            <View style={styles.itemFooterInner}>
                                <Text style={styles.itemFooterTitle}>Total</Text>

                                <Text style={styles.itemFooterTotal}>{numeral(this.getTotal()).format('$ 0,0.00')}</Text>
                            </View>
                        </View>
                    </View>
                </View>

                <View style={styles.noComissionContainer}>
                    <Radio
                        value={!this.state.payComission}
                        onPress={() => this.togglePayComission()}
                        color={colors.tertiaryColor}
                    />

                    <TouchableOpacity onPress={() => this.togglePayComission()}>
                        <Text style={styles.noComissionText}>Não quero pagar a Taxa de Serviço</Text>
                    </TouchableOpacity>
                </View>
            </View>
        );
    }
}

styles = {
    innerContainer: {
        flexDirection: 'row',
        marginVertical: 16,
        width: '100%',
    },
    comandaContainer: {
        backgroundColor: colors.secondaryColor,
        justifyContent: 'center',
        alignItems: 'center',
        position: 'relative',
        padding: 40,
    },
    comandaNumber: {
        color: colors.textInSecondary,
        fontFamily: fonts.secondary,
        fontSize: 20,
        textAlign: 'center',
    },
    check: {
        position: 'absolute',
        left: 10,
        top: 10,
    },
    itemsContainer: {
        flex: 1,
    },
    itemRow: {
        flexDirection: 'row',
        width: '100%',
    },
    item: {
        padding: 15,
        borderWidth: 3,
        borderColor: '#dfdbd8',
        fontFamily: fonts.secondary,
        color: colors.defaultText,
        fontSize: 18,
    },
    itemQuantity: {
        textAlign: 'center',
        flex: 5,
    },
    itemName: {
        flex: 85,
    },
    itemPrice: {
        textAlign: 'right',
        flex: 10,
    },
    itemFooter: {
        backgroundColor: '#e2d8d2',
        flex: 1,
        padding: 20,
        justifyContent: 'center',
    },
    itemFooterInner: {
        // marginLeft: wp('5%'),
        flexDirection: 'row',
        justifyContent: 'space-between',
    },
    itemFooterSubtitle: {
        fontFamily: fonts.default,
        fontSize: 14,
        color: '#787878',
    },
    itemFooterSubtotal: {
        fontFamily: fonts.default,
        fontSize: 14,
        color: '#787878',
        textAlign: 'right',
    },
    itemFooterTitle: {
        fontFamily: fonts.default,
        fontSize: 18,
        color: colors.defaultText,
    },
    itemFooterTotal: {
        fontFamily: fonts.default,
        fontSize: 18,
        color: colors.defaultText,
        textAlign: 'right',
    },
    itemCanceled: {
        color: '#D8D8D8',
    },
    noComissionContainer: {
        flexDirection: 'row',
        marginVertical: 5,
        alignItems: 'center',
    },
    noComissionText: {
        fontFamily: fonts.secondary,
        color: colors.defaultText,
        fontSize: 18,
        marginLeft: 10,
    }
}