import React, {Component} from 'react';
import { Text, View, TouchableOpacity, Alert, FlatList } from 'react-native';

import Comanda from '../models/Comanda';

export default class CloseAccountItem extends Component {

    constructor(props) {
        super(props);

        this.state = {
            
        };
    }

    componentDidMount() {
        console.log(this.props.products);
    }

    render() {
        return (
            <View style={styles.container}>
                <FlatList 
                    numColumns={4}
                    data={this.props.products}
                    keyExtractor={(i, key) => key.toString()}
                    renderItem={p => 
                        <View>
                            <Text style={styles.item}>{p.item.quantity}</Text>
                            <Text style={styles.item}>{p.item.nome}</Text>
                            <Text style={styles.item}>{p.item.value}</Text>
                        </View>
                    }
                />
            </View>
        );
    }
}

styles = {
    container: {
        margin: 10,
    }
}