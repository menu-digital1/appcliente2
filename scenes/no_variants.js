import React, { Component } from 'react';
import { Text, View, FlatList } from 'react-native';
import { connect } from 'react-redux';
import _ from 'lodash';
import ImageLoad from 'react-native-image-placeholder';
import numeral from 'numeral';

import Btn from '../components/btn';
import { colors } from '../styles/colors';
import { fonts } from '../styles/app';
import NoVariantItem from './no_variant_item';
import { updateComandas } from '../reducers/actions';

class NoVariants extends Component {
    constructor(props) {
        super(props);

        this.state = {};
    }

    finalize() {
        this.props.closeModal();
    }

    render() {
        return (
            <View>
                <View style={styles.productContainer}>
                    <ImageLoad
                        source={{
                            uri: this.props.product.full_gif ? this.props.product.full_gif : this.props.product.full_imagem
                        }}
                        isShowActivity={false}
                        style={styles.productImage}
                    />

                    <View style={styles.productInfo}>
                        <Text style={styles.productTitle}>{this.props.product.nome}</Text>

                        <Text style={styles.productDescription}>{this.props.product.descricao}</Text>

                        <Text style={styles.productValue}>
                            Valor unitário:{' '}
                            <Text style={{ fontWeight: 'bold' }}>{numeral(this.props.product.valor_final).format('$ 0,0.00')}</Text>
                        </Text>
                    </View>
                </View>

                <Text style={styles.instructions}>Selecione a quantidade:</Text>

                <FlatList
                    extraData={this.state}
                    data={this.props.comandas}
                    style={styles.comandasList}
                    keyExtractor={(i, key) => key.toString()}
                    renderItem={({ item, index }) => <NoVariantItem comanda={item} index={index} product={this.props.product} />}
                />

                <View style={{ marginTop: 40 }}>
                    <Btn text='FINALIZAR' onPress={() => this.finalize()} />
                </View>
            </View>
        );
    }
}

export default connect(
    state => ({
        comandas: state.comandasReducer.comandas
    }),
    { updateComandas }
)(NoVariants);

const styles = {
    container: {
        alignSelf: 'stretch',
        backgroundColor: '#fff',
        borderRadius: 5
    },
    productInfoContainer: {
        flexDirection: 'row',
        justifyContent: 'space-between',
        padding: 10
    },
    productContainer: {
        flexDirection: 'row',
        borderRadius: 2
    },
    productImage: {
        width: 192,
        minHeight: 144,
        backgroundColor: colors.secondaryColor,
        resizeMode: 'contain'
    },
    productInfo: {
        backgroundColor: '#F7F2F2',
        padding: 16,
        flex: 1
    },
    productTitle: {
        color: colors.defaultText,
        fontSize: 18,
        fontFamily: fonts.secondary,
        marginBottom: 16
    },
    productDescription: {
        color: colors.defaultText,
        fontSize: 14,
        fontFamily: fonts.default,
        marginBottom: 16
    },
    productValue: {
        color: colors.defaultText,
        fontSize: 14,
        fontFamily: fonts.default,
        fontWeight: 'bold'
    },
    comandasList: {
        maxHeight: 300
    },
    comandaItem: {
        paddingVertical: 15,
        paddingHorizontal: 40,
        borderWidth: 1,
        borderColor: '#ebe4e0',
        flexDirection: 'row',
        alignItems: 'center'
    },
    comandaNumber: {
        color: colors.secondaryColor,
        fontSize: 20,
        fontFamily: fonts.secondary,
        flex: 1
    },
    commandaSpinner: {
        flex: 1,
        textAlign: 'center'
    },
    comandaValue: {
        textAlign: 'right',
        fontSize: 22,
        fontFamily: fonts.default,
        color: colors.defaultText,
        fontWeight: 'bold',
        flex: 1
    },
    closeIcon: {
        color: '#BABABA',
        fontSize: 26
    },
    waiterCallContainer: {
        marginRight: 60,
        marginLeft: 15
    },
    instructions: {
        fontFamily: fonts.default,
        fontSize: 14,
        color: colors.defaultText,
        marginTop: 15,
        marginBottom: 5
    }
};
