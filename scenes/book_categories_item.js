import React, { Component } from "react";
import { Text, View, TouchableOpacity } from "react-native";

import { colors } from "../styles/colors";
import { fonts } from "../styles/app";
import MenuButton from "../components/menu_button";

export default class BookCategoriesItem extends Component {
    render() {
        return (
            <MenuButton
                text={this.props.category[0]}
                onPress={() => this.props.findBooks(this.props.category[0])}
                active={this.props.currentCategory == this.props.category[0]}
            />
        );
    }
}

const styles = {
    button: {
        padding: 16,
        fontFamily: fonts.secondary,
        color: colors.defaultText,
        fontSize: 20,
        textAlign: "center",
        marginVertical: 16,
        alignItems: "center",
        height: 56,
        borderRadius: 2
    }
};
