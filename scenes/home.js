import React, { Component } from 'react';
import { Text, View, Modal, Alert, Image, TouchableOpacity, TouchableWithoutFeedback } from 'react-native';
import t from 'tcomb-form-native';
import { Actions } from 'react-native-router-flux';
import Carousel from 'react-native-snap-carousel';
import Icon from 'react-native-vector-icons/FontAwesome';
import { connect } from 'react-redux';

import Comanda from '../models/Comanda';
import Btn from '../components/btn';
import inputShowPasswordTemplate from '../components/input-show-password';
import Settings from '../models/Settings';
import Banner from '../models/Banner';
import BannerItem from './banner_item';
import InternetMonitor from './internet_monitor';
import OrdersBar from '../components/order_bar';
import { colors } from '../styles/colors';
import { fonts, app } from '../styles/app';
import DonutLoading from '../components/donut_loading';
import Table from '../models/Table';
import Api from '../services/api';
import MenuButton from '../components/menu_button';
import TableBar from '../components/table_bar';
import SmallModal from '../components/small_modal';

const Form = t.form.Form;

class Home extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalVisible: false,
            user: null,
            authenticating: false,
            banners: [],
            noBanners: false,
            loading: false,
            bannerWidth: 1
        };

        this.User = t.struct({
            email: t.maybe(t.String),
            password: t.maybe(t.String)
        });

        this.formOptions = {
            fields: {
                email: {
                    placeholder: 'E-mail',
                    returnKeyType: 'next',
                    textContentType: 'emailAddress',
                    keyboardType: 'email-address',
                    autoCapitalize: 'none',
                    placeholderTextColor: '#C4BCB6',
                    onSubmitEditing: () => this._form.getComponent('password').refs.input.refs.input.focus()
                },
                password: {
                    placeholder: 'Senha',
                    template: inputShowPasswordTemplate,
                    placeholderTextColor: '#C4BCB6',
                    onSubmitEditing: () => this.authenticate(),
                    returnKeyType: 'go'
                }
            },
            stylesheet: {
                ...Form.stylesheet,
                textbox: {
                    normal: {
                        borderWidth: 1,
                        borderColor: '#C4BCB6',
                        borderRadius: 2,
                        padding: 15,
                        fontFamily: fonts.default,
                        fontSize: 20,
                        color: colors.defaultText,
                        marginBottom: 16
                    }
                },
                controlIcon: {
                    container: {
                        normal: {
                            borderWidth: 1,
                            borderColor: '#C4BCB6',
                            borderRadius: 2,
                            padding: 15,
                            marginBottom: 16
                        }
                    },

                    input: {
                        normal: {
                            fontFamily: fonts.default,
                            fontSize: 20,
                            color: colors.defaultText
                        }
                    },

                    button: {
                        normal: {
                            color: colors.defaultText,
                            fontSize: 20
                        }
                    }
                }
            },
            auto: 'none'
        };
    }

    componentDidMount() {
        this.setState({
            loading: true
        });

        Banner.find().then(banners => {
            console.log(banners);
            this.setState({
                banners,
                loading: false,
                noBanners: banners.length == 0
            });
        });
    }

    openSettings() {
        this.setState({ modalVisible: true });
    }

    authenticate() {
        this.setState({ authenticating: true });

        var email = this.state.user ? this.state.user.email : '';
        var password = this.state.user ? this.state.user.password : '';

        Settings.authenticate(email, password).then(
            () => {
                this.closeModal();

                Actions.settings();
            },
            error => {
                Alert.alert(
                    'ERRO',
                    error,
                    [
                        {
                            text: 'OK',
                            onPress: () => this.setState({ authenticating: false })
                        }
                    ],
                    { cancelable: false }
                );
            }
        );
    }

    closeModal() {
        this.setState({ modalVisible: false });
    }

    callWaiter() {
        Alert.alert('AGUARDE', 'O garçom foi chamado e virá até a sua mesa logo.', [{ text: 'OK' }], {
            cancelable: false
        });

        Table.getTable().then(table => {
            Api.get('call-waiter/' + table);
        });
    }

    render() {
        return (
            <View style={app.mainContainer}>
                <InternetMonitor />

                <View style={!this.state.noBanners ? app.menuContainer : styles.menuContainerFull}>
                    <TableBar />

                    <View style={app.menuContainerInner}>
                        <TouchableOpacity
                            onPress={() => this.openSettings()}
                            hitSlop={{
                                top: 40,
                                left: 40,
                                bottom: 40,
                                right: 40
                            }}
                            style={styles.settingsButton}
                        >
                            <Icon name='cog' style={styles.settingsIcon} />
                        </TouchableOpacity>

                        <Image source={require('../img/logo_limpa.png')} style={!this.state.noBanners ? styles.logo : styles.logoFull} />

                        <View style={!this.state.noBanners ? styles.menu : styles.menuFull}>
                            {this.props.comandas.length !== 0 && (
                                <View>
                                    <MenuButton text='Cardápio' icon='cutlery' onPress={() => Actions.products()} />
                                    <MenuButton text='Livros' icon='book' onPress={() => Actions.books()} />
                                    <MenuButton text='Eventos' icon='calendar' onPress={() => Actions.events()} />
                                    <MenuButton text='Minha Conta' icon='usd' onPress={() => Actions.closeAccount()} />
                                    <MenuButton text='Comandas' icon='qrcode' onPress={() => Actions.comandas()} />
                                </View>
                            )}

                            {this.props.comandas.length === 0 && (
                                <View>
                                    <Text style={styles.introText}>BEM-VIND@!</Text>

                                    <Text style={[styles.introText, { marginBottom: 16 }]}>
                                        Antes de começar, insira a(s) comanda(s) através do botão abaixo:
                                    </Text>

                                    <MenuButton text='Comandas' icon='qrcode' onPress={() => Actions.enterComanda()} />
                                </View>
                            )}
                        </View>
                    </View>
                </View>

                {!this.state.noBanners && (
                    <View style={styles.content}>
                        <OrdersBar />

                        <View style={{ flex: 1 }}>
                            {!this.state.loading && (
                                <View
                                    style={{ flex: 1 }}
                                    onLayout={event =>
                                        this.setState({
                                            bannerWidth: event.nativeEvent.layout.width
                                        })
                                    }
                                >
                                    <Carousel
                                        data={this.state.banners}
                                        renderItem={({ item, index }) => <BannerItem banner={item} hasComandas={this.props.comandas.length !== 0} />}
                                        sliderWidth={this.state.bannerWidth}
                                        itemWidth={this.state.bannerWidth}
                                        loop
                                        autoplay
                                        enableMomentum={false}
                                        lockScrollWhileSnapping
                                        autoplayInterval={5000}
                                    />
                                </View>
                            )}

                            {this.state.loading && <DonutLoading />}
                        </View>
                    </View>
                )}

                <SmallModal visible={this.state.modalVisible} closeModal={() => this.closeModal()}>
                    <Image source={require('../img/padlock.png')} style={styles.modalImg} />

                    <Text style={styles.modalTitle}>ACESSO RESTRITO</Text>

                    <Form
                        type={this.User}
                        options={this.formOptions}
                        ref={c => (this._form = c)}
                        value={this.state.user}
                        onChange={user => this.setState({ user })}
                    />

                    <Btn text='AUTENTICAR' loading={this.state.authenticating} onPress={() => this.authenticate()} />
                </SmallModal>
            </View>
        );
    }
}

export default connect(
    state => ({
        comandas: state.comandasReducer.comandas
    }),
    {}
)(Home);

const styles = {
    container: {
        flex: 1,
        height: '100%',
        flexDirection: 'row'
    },
    menuContainer: {
        height: '100%',
        backgroundColor: colors.primaryColor,
        paddingHorizontal: 40,
        paddingBottom: 40,
        paddingTop: 120,
        position: 'relative',
        width: 400,
        justifyContent: 'center'
    },
    menuContainerFull: {
        height: '100%',
        backgroundColor: colors.primaryColor,
        position: 'relative',
        flex: 1
    },
    content: {
        flex: 1,
        backgroundColor: colors.background
    },
    logo: {
        height: 184,
        resizeMode: 'contain',
        alignSelf: 'center',
        marginBottom: 16
    },
    logoFull: {
        height: 160,
        width: '100%',
        resizeMode: 'contain',
        alignSelf: 'center',
        marginBottom: 16
    },
    menu: {},
    menuFull: {
        marginVertical: 25,
        // marginHorizontal: wp('30%')
    },
    menuButton: {
        backgroundColor: '#fff',
        flexDirection: 'row',
        alignItems: 'center',
        paddingVertical: 15,
        paddingHorizontal: 60,
        marginBottom: 15
    },
    menuIcon: {
        color: colors.defaultText,
        fontSize: 18,
        marginRight: 8
    },
    menuText: {
        color: colors.defaultText,
        fontSize: 18,
        fontFamily: fonts.secondary,
        marginLeft: 25
    },
    settingsButton: {
        position: 'absolute',
        bottom: 16,
        left: 16
    },
    settingsIcon: {
        color: colors.textInPrimary,
        fontSize: 32
    },
    modalContent: {
        // paddingHorizontal: wp('25%'),
        justifyContent: 'center',
        alignItems: 'center',
        flex: 1,
        backgroundColor: 'rgba(0, 0, 0, 0.8)'
    },
    modalBody: {
        backgroundColor: '#fff',
        padding: 30,
        borderRadius: 5,
        position: 'relative'
    },
    modalImg: {
        resizeMode: 'contain',
        height: 88,
        marginBottom: 16,
        alignSelf: 'center'
    },
    modalTitle: {
        fontFamily: fonts.default,
        fontSize: 26,
        textAlign: 'center',
        color: colors.defaultText,
        marginVertical: 16,
        fontWeight: 'bold'
    },
    closeButton: {
        position: 'absolute',
        top: 10,
        right: 10
    },
    closeIcon: {
        color: '#BABABA',
        fontSize: 26
    },
    introText: {
        fontFamily: fonts.secondary,
        fontSize: 20,
        color: colors.onPrimary
    }
};
