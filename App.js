/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React, { Component } from 'react';
import Orientation from 'react-native-orientation';
import { Router, Scene } from 'react-native-router-flux';
import { Provider } from 'react-redux';

import Table from './models/Table';

import ChooseTable from './scenes/choose_table';
import EnterComanda from './scenes/enter_comanda';
import Home from './scenes/home';
import Comandas from './scenes/comandas';
import CloseAccount from './scenes/close_account';
import Settings from './scenes/settings';
import Books from './scenes/books';
import Products from './scenes/products';
import store from './components/store';
import Events from './scenes/events';
import SuccessOrder from './scenes/success_order';

export default class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            loading: true,
            hasTable: false,
            hasComandas: false
        };
    }

    componentDidMount() {
        Orientation.lockToLandscape();

        Table.hasTable().then(hasTable => {
            this.setState({
                hasTable,
                loading: false
            });
        });
    }

    render() {
        if (this.state.loading) return null;

        return (
            <Provider store={store}>
                <Router>
                    <Scene key='root' hideNavBar>
                        <Scene key='chooseTable' component={ChooseTable} initial={!this.state.hasTable}></Scene>
                        <Scene key='home' component={Home} initial={this.state.hasTable}></Scene>
                        <Scene key='enterComanda' component={EnterComanda}></Scene>
                        <Scene key='comandas' component={Comandas}></Scene>
                        <Scene key='closeAccount' component={CloseAccount}></Scene>
                        <Scene key='settings' component={Settings}></Scene>
                        <Scene key='books' component={Books}></Scene>
                        <Scene key='products' component={Products}></Scene>
                        <Scene key='events' component={Events}></Scene>
                        <Scene key='successOrder' component={SuccessOrder}></Scene>
                    </Scene>
                </Router>
            </Provider>
        );
    }
}
