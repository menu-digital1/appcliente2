export const colors = {
	background: '#F1EBE7',
	surface: '#fff',

	primary: '#AB1E35',
	secondary: '#f39200',
	tertiary: '#2980B9',

	onSurface: '#2A2A2A',
	onPrimary: '#fff',
	onSecondary: '#2A2A2A',
	onTertiary: '#fff',

	defaultText: '#2A2A2A',
	divider: 'rgba(0, 0, 0, 0.12)',

	lightGray: '#C4BCB6',
	gray: '#DFDBD8',

	primaryColor: '#AB1E35',
	secondaryColor: '#f39200',
	secondaryLightColor: '#F5A52E',
	tertiaryColor: '#2980B9',
	tertiaryLightColor: '#9DC5DF',

	textInPrimary: '#fff',
	textInSecondary: '#fff',
	textInTertiary: '#fff'
};
