import { colors } from './colors';

export const fonts = {
	default: 'Roboto-Regular',
	secondary: 'Viga-Regular'
};

export const app = {
	blankContainer: {
		flex: 1,
		backgroundColor: '#fff',
		padding: 20
	},
	mainContainer: {
		flex: 1,
		flexDirection: 'row'
	},
	menuContainer: {
		height: '100%',
		backgroundColor: colors.primary,
		width: 400
	},
	menuContainerInner: {
		paddingHorizontal: 40,
		paddingBottom: 40,
		flex: 1,
		justifyContent: 'center'
	},
	mainContent: {
		flex: 1,
		backgroundColor: colors.background,
		paddingHorizontal: 40,
		position: 'relative',
	},
	mainHeader: {
		flexDirection: 'row',
		justifyContent: 'space-between',
		alignItems: 'center',
		marginTop: 32,
		marginBottom: 16
	},
	h2: {
		color: colors.defaultText,
		fontSize: 30,
		fontFamily: fonts.secondary
	}
};
