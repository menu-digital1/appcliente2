import Api from "../services/api";

export default class Banner {
    static find() {
        return new Promise((resolve, reject) => {
            Api.get('banners').then(banners => {
                resolve(banners);
            });
        });
    }
}