import Api from "../services/api";

export default class Order {
    static sendOrder(data) {
        return new Promise((resolve, reject) => {
            Api.post('pedido', data).then(response => {
                if (response.error == false) {
                    resolve(true);
                } else {
                    reject(response);
                }
            });
        });
    }

    static getOrders(comandas) {
        var buffer = [];
        for (var c of comandas) {
            buffer.push(c.number);
        }

        data = {
            comandas: buffer,
        };

        return new Promise((resolve, reject) => {
            Api.post('orders', data).then(orders => {
                resolve(orders);
            });
        });
    }
}