import NetInfo from "@react-native-community/netinfo";

export default class Network {
    static monitor(callback) {
        NetInfo.addEventListener(state => {
            callback(state.isConnected);
        });
    }
}