import { Actions } from "react-native-router-flux";

import Table from "./Table";
import Comanda from "./Comanda";

export default class Route {
    static homeRoute() {
        Table.hasTable().then(hasTable => {
            if (hasTable) {
                if (Comanda.hasComandas()) {
                    Actions.reset('home');
                } else {
                    Actions.reset('enterComanda');
                }
            } else {
                Actions.reset('chooseTable');
            }
        });
    }
}