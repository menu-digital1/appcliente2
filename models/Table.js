import AsyncStorage from "@react-native-community/async-storage";
import Api from "../services/api";

export default class Table {
    static TABLE_NUMBER = "br.com.agenciamaior.menu_digital.table_number";

    static hasTable() {
        return new Promise(resolve => {
            AsyncStorage.getItem(this.TABLE_NUMBER).then(value => {
                if (value) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            });
        });
    }

    static getTable() {
        return new Promise((resolve, reject) => {
            AsyncStorage.getItem(this.TABLE_NUMBER).then(value => {
                if (value) {
                    resolve(value);
                } else {
                    resolve(null);
                }
            });
        });
    }

    static setTable(tableNumber) {
        return new Promise(resolve => {
            Api.get("table/link/" + tableNumber).then(
                response => {
                    if (response.error == false) {
                        AsyncStorage.setItem(this.TABLE_NUMBER, tableNumber).then(() => {});
                        resolve(response);
                    } else {
                        resolve(response);
                    }
                    //resolve(true);
                },
                error => {
                    resolve({ error: true, message: "Ocorreu um erro inesperado" });
                }
            );
        });
    }

    static clearTable() {
        return new Promise((resolve, reject) => {
            this.getTable().then(table => {
                AsyncStorage.removeItem(this.TABLE_NUMBER).then(() => {
                    Api.get("table/unlink/" + table).then(
                        response => {
                            resolve(response);
                        },
                        error => {
                            reject({ error: true, message: "Ocorreu um erro inesperado" });
                        }
                    );
                });
            });
        });
    }
}
