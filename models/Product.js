import Api from "../services/api";

export default class Product {
    static findCategories() {
        return new Promise((resolve, reject) => {
            Api.get('cardapio').then(categories => {
                resolve(categories);
            });
        });
    }

    static findProducts(subcategoryId) {
        return new Promise((resolve, reject) => {
            Api.post('cardapio/produtos/' + subcategoryId).then(products => {
                resolve(products);
            });
        });
    }
}