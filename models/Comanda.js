import _ from 'lodash';

import Api from '../services/api';
import store from '../components/store';
import { updateComandas } from '../reducers/actions';

export default class Comanda {
    constructor() {
        this.number = 0;
        this.products = [];
    }

    static hasComandas() {
        return this.getComandas().length !== 0;
    }

    static getComandas() {
        return _.sortBy(store.getState().comandasReducer.comandas, c => c.number);
    }

    static setComandas(comandas) {
        store.dispatch(updateComandas(comandas));
    }

    static addComanda(commandaNumber, tableNumber) {
        return new Promise((resolve, reject) => {
            commandaNumber = Number(commandaNumber);
            
            var comandas = this.getComandas();

            var data = {
                code: commandaNumber,
                table: tableNumber,
            };
    
            Api.post('link-comanda', data).then(() => {
                if (!_.find(comandas, c => c.number == commandaNumber)) {        
                    comandas.push({ number: commandaNumber });
        
                    this.setComandas(comandas);

                    resolve(true);
                } else {
                    resolve(true);
                }
            });
        });
    }

    static removeComanda(commandaNumber) {
        return new Promise((resolve, reject) => {
            var comandas = this.getComandas();
    
            comandas = _.reject(comandas, c => c.number == commandaNumber);
    
            this.setComandas(comandas);

            var data = {
                code: commandaNumber,
            };

            Api.post('unlink-comanda', data).then(() => {
                resolve(true);
            });
        });
    }

    static checkComanda(commandaNumber, tableNumber) {
        return new Promise((resolve, reject) => {
            var data = {
                code: commandaNumber,
                table: tableNumber,
            };

            Api.post('check-comanda/', data).then(response => {
                if (response.error == true) {
                    reject(response.message);
                } else {
                    resolve(response);
                }
            });
        });
    }

    static clearComandas(tableNumber) {
        return new Promise((resolve, reject) => {
            this.setComandas([]);
    
            Api.post('clear-comandas', { table: tableNumber }).then(() => {
                resolve(true);
            });
        });
    }
    
    static getOrders(commandaNumber) {
        return new Promise((resolve, reject) => {
            Api.post('checkout/ordersby_comanda', { commandaNumber: commandaNumber }).then(response => {
                resolve(response);
            });
        });
    }

    static updateComanda(comanda) {
        var comandas = this.getComandas();

        var index = _.findIndex(comandas, c => c.number == comanda.number);

        comandas[index] = comanda;

        this.setComandas(comandas);
    }
}