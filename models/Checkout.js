import Api from "../services/api";

export default class Checkout {
    static pedirConta(comandas, table) {
        return new Promise((resolve, reject) => {
            Api.post('checkout/pedir_conta', { comandas, table }).then(response => {
                resolve(response);
            });
        });
    }
}