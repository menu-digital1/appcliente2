import Api from "../services/api";

export default class Settings {
    static authenticate(email, password) {
        return new Promise((resolve, reject) => {
            var data = { email, password };

            Api.post('auth', data).then(response => {
                if (!response.error) {
                    resolve(true);
                } else {
                    reject(response.message);
                }
            });
        });
    }
}