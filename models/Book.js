import Api from "../services/api";

export default class Book {
    static findCategories() {
        return new Promise((resolve, reject) => {
            Api.get('books/categories').then(categories => {
                resolve(categories);
            });
        });
    }

    static find(searchParams) {
        return new Promise((resolve, reject) => {
            Api.post('books', searchParams).then(books => {
                resolve(books);
            });
        });
    }
}